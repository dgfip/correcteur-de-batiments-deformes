# syntax=docker/dockerfile:1

# Need to mount to get the output (output will be own by root:root)
# e.g.
# docker build --tag buildings_corrector .
# docker run --mount type=bind,source=$(pwd),target=/mnt buildings_corrector evolution -o /mnt/csv/test.csv -n 10

FROM python:3.8
RUN apt-get update
RUN apt-get install ffmpeg libsm6 libxext6 -y  # for opencv
WORKDIR /app
RUN python3 -m venv venv
COPY requirements.txt requirements.txt
RUN venv/bin/pip install --upgrade pip
RUN venv/bin/pip install -r requirements.txt
COPY main.py main.py
COPY evolution/*.py evolution/
COPY heuristics/*.py heuristics/
COPY LSD/*.py LSD/
COPY utils/*.py utils/
COPY input/example.* input/
COPY jp2/example.* jp2/
RUN mkdir csv
ENV PROJ_LIB=venv/lib/python3.8/site-packages/rasterio/proj_data

ENTRYPOINT [ "venv/bin/python", "main.py"]

