#  Copyright (c) 2021-2021. DGFiP (Direction Générale des Finances Publiques)
#
#  This software is a computer program whose purpose is to find a good
#  representation of buildings from an imprecise one.
#
#  This software is governed by the CeCILL  license under French law and
#  abiding by the rules of distribution of free software.  You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  "http://www.cecill.info".
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using,  modifying and/or developing or reproducing the
#  software by the user in light of its specific status of free software,
#  that may mean  that it is complicated to manipulate,  and  that  also
#  therefore means  that it is reserved for developers  and  experienced
#  professionals having in-depth computer knowledge. Users are therefore
#  encouraged to load and test the software's suitability as regards their
#  requirements in conditions enabling the security of their systems and/or
#  data to be ensured and,  more generally, to use and operate it in the
#  same conditions as regards security.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL license and that you accept its terms.


from typing import Tuple, Optional

from shapely import geometry
import numpy as np


def sticked_coeff(points: np.ndarray) -> Tuple[float, Optional[np.ndarray], Optional[np.ndarray]]:
    """
    Calculates a coefficient representing how a polygon could be several buildings
    :param points: Exterior points of the polygon as linestring (first != last)
    :return: the coefficient and the two most likely best points to split the polygon in half
    """
    coeff, p1, p2 = 0.0, None, None
    for i, pt in enumerate(points):
        for j in range(i+1, len(points)):
            other = points[j]
            dist = np.linalg.norm(pt - other)
            if dist < 3.0:
                dist_line = geometry.LineString(np.concatenate((points[j:], points[:i+1]))).length
                dist_line = min(dist_line, geometry.LineString(points[i:j+1]).length)
                points_coeff = dist_line / dist
                if points_coeff > coeff:
                    coeff, p1, p2 = points_coeff, pt, other
    return coeff, p1, p2


def split_sticked(reference: geometry.Polygon,
                  p1: geometry.Point,
                  p2: geometry.Point) -> Optional[geometry.MultiPolygon]:
    """
    Splits reference in half using the two points, works only if there is exactly two polygons at the end
    :param reference: polygon to split
    :param p1: base point for splitting
    :param p2: base point for splitting
    :return: Two polygons or None if can't split
    """
    splitter = geometry.LineString([p1, p2]).buffer(0.5)
    result = reference.difference(splitter)
    if result.geom_type == 'MultiPolygon' and len(result.geoms) == 2:
        return result
