#  Copyright (c) 2021-2021. DGFiP (Direction Générale des Finances Publiques)
#
#  This software is a computer program whose purpose is to find a good
#  representation of buildings from an imprecise one.
#
#  This software is governed by the CeCILL  license under French law and
#  abiding by the rules of distribution of free software.  You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  "http://www.cecill.info".
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using,  modifying and/or developing or reproducing the
#  software by the user in light of its specific status of free software,
#  that may mean  that it is complicated to manipulate,  and  that  also
#  therefore means  that it is reserved for developers  and  experienced
#  professionals having in-depth computer knowledge. Users are therefore
#  encouraged to load and test the software's suitability as regards their
#  requirements in conditions enabling the security of their systems and/or
#  data to be ensured and,  more generally, to use and operate it in the
#  same conditions as regards security.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL license and that you accept its terms.


import math
from typing import Optional, Union, Dict, List

import geojson
import numpy as np
import shapefile
from shapely import geometry
from shapely.geometry import shape

Geometry = Optional[Union[geometry.Polygon, geometry.MultiPolygon]]

SQRT2_2 = math.sqrt(2.0)/2.0
SQRT3_2 = math.sqrt(3.0)/2.0


def norm_square(v):
    return np.dot(v, v)


def calculate_intersection(p1: np.ndarray, v1: np.ndarray, p2: np.ndarray, v2: np.ndarray) -> Optional[np.array]:
    """
    Intersection between two lines
    :param p1: Line 1 point
    :param v1: Line 1 vector
    :param p2: Line 2 point
    :param v2: Line 2 Vector
    :return: Intersection point
    """
    n1 = np.array([v1[1], -v1[0]])
    n2 = np.array([v2[1], -v2[0]])
    matrix = np.array([n1, n2])
    b = np.array([np.dot(n1, p1), np.dot(n2, p2)])
    try:
        return np.linalg.solve(matrix, b)
    except np.linalg.LinAlgError:
        return None


def calculate_iou(geom1: Geometry, geom2: Geometry) -> float:
    """
    Intersection over Union for two geometries
    :param geom1: first one
    :param geom2: second one
    :return: IoU
    """
    a2 = geom1.union(geom2).area
    return geom1.intersection(geom2).area / a2 if a2 > 1.0 else 0.0


def distance_max(line: geometry.LineString, other: geometry.LinearRing) -> float:
    """
    Maximal distance between points of a segment and a geometry
    :param line: Segment to analyse
    :param other: Reference geoemtry
    :return: Calculate le largest distance with 1m steps
    """
    p0 = np.array(line.coords[0])
    d_max = 0.0
    for p1 in np.array(line.coords[1:]):
        v = p1 - p0
        nb_step = 0
        step = 0
        if norm_square(p0 - p1) > 0.001:
            factor = 2.0
            length = np.linalg.norm(v)
            v /= length
            nb_step = math.ceil(factor*length)
            step = length / nb_step
        d_max = max(d_max, max(geometry.Point(p0 + i * step * v).distance(other) for i in range(nb_step + 1)))
    return d_max


def list_angles(simple_geom: geometry.Polygon) -> List[float]:
    """
    List of angles between vectors before and after each point of a polygon
    :param simple_geom: Geometry to analyse
    :return: List of angles in radians between -Pi an Pi
    """
    angles = [0.0] * (len(simple_geom.exterior.coords) - 1)
    prec = None
    v0 = None
    for i, p in enumerate(np.array(simple_geom.exterior.coords)):
        if prec is None:
            prec = p
        elif v0 is None:
            v0 = p - prec
            norm0 = np.linalg.norm(v0)
            if norm0 > 0.01:
                v0 /= norm0
            else:
                v0 = None
            prec = p
        else:
            v1 = p - prec
            norm1 = np.linalg.norm(v1)
            if norm1 > 0.01:
                v1 /= norm1
            angles[i - 1] = np.arccos(np.dot(v0, v1)) * np.sign(np.linalg.det([v0, v1]))
            prec = p
            v0 = v1
    # Special case for first/last point
    p0 = np.array(simple_geom.exterior.coords[0])
    p1 = np.array(simple_geom.exterior.coords[1])
    pn = np.array(simple_geom.exterior.coords[-2])
    v0 = p0 - pn
    v1 = p1 - p0
    norm0 = np.linalg.norm(v0)
    norm1 = np.linalg.norm(v1)
    if norm0 > 0.01 and norm1 > 0.01:
        v0 /= norm0
        v1 /= norm1
        angles[0] = np.arccos(np.dot(v0, v1)) * np.sign(np.linalg.det([v0, v1]))
    return angles


def list_turns(simple_geom: geometry.Polygon) -> List[float]:
    """
    Produce le list of 1.0 or -1.0 depending on the sens of rotation (left or right) at the same index point
    :param simple_geom: The geometry to analyse, last point (== first point) not analysed
    :return: list of 1.0/-1.0
    """
    turns = [0.0] * (len(simple_geom.exterior.coords) - 1)  # turn direction (left/right) for each point
    prec = None
    v0 = None
    for i, p in enumerate(np.array(simple_geom.exterior.coords)):
        if prec is None:
            prec = p
        elif v0 is None:
            v0 = p - prec
        else:
            v1 = p - prec
            turns[i - 1] = np.sign(np.linalg.det([v0, v1]))  # cross product => sens of rotation
            prec = p
            v0 = v1
    # Special case for first/last point
    p0 = np.array(simple_geom.exterior.coords[0])
    p1 = np.array(simple_geom.exterior.coords[1])
    pn = np.array(simple_geom.exterior.coords[-2])
    turns[0] = np.sign(np.linalg.det([p0 - pn, p1 - p0]))
    return turns


def read_shapefile(file: str, key: str) -> Dict[str, geometry.Polygon]:
    with shapefile.Reader(file) as buildings_shp:
        buildings_geom = [(r.record[key], shape(r.shape.__geo_interface__)) for r in buildings_shp.shapeRecords()]
    return {r[0]: r[1] for r in buildings_geom if r[1].is_valid}


def read_geojson(file: str, key: str) -> Dict[str, geometry.Polygon]:
    with open(file) as f:
        gj = geojson.load(f)
    buildings_geom: Dict[str, geometry.Polygon] = {}
    for feature in gj['features']:
        polygon = shape(feature["geometry"]).buffer(0)
        if polygon.is_valid:
            buildings_geom[feature['properties'][key]] = polygon
    return buildings_geom


def read_file(file: str, key: str = 'object_id') -> Dict[str, geometry.Polygon]:
    """
    Reads a polygon file
    :param file: file name (shapefile or geojson)
    :param key: key indexing the geometries
    :return:
    """
    if file.endswith('.shp') or file.endswith('.SHP'):
        return read_shapefile(file, key)
    elif file.endswith('.geojson') or file.endswith('.json'):
        return read_geojson(file, key)
    print(f'Can\'t read {file}')
    exit(1)
