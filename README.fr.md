# Présentation

Starting from the geometries of buildings extracted from an aerial image and ill-cropped,
this script seeks to offer geometries more consistent with what a building is supposed to be.

Par exemple : 

Partant de et arrivant à...

![starting from](before.png "Starting from...") ![come up with](after.png "Come up with...")

Pour cela, différentes heuristiques sont utilisées.
Ce script est dans un état expérimental


# Utilisation

## Installation

```shell
$ git clone git@gitlab.adullact.net:dgfip/correcteur-de-batiments-deformes.git
$ cd correcteur-de-batiments-deformes
$ python3.8 -m venv venv
$ echo 'export PROJ_LIB=${VIRTUAL_ENV}/lib/python3.8/site-packages/rasterio/proj_data' >> venv/bin/activate  ## Pour éviter certains avertissements
$ source venv/bin/activate
$ pip install -r requirements.txt
$ mkdir csv  # Ou un autre répertoire pour les résultats
```

## Script principal

Le script principal est "main.py", un script python3 (testé avec la v3.8)
prenant en argument le nom de l'algorithme à utiliser.

```
$ python3 main.py -h
usage: main.py [-h] {evolution,lsd,post-processing,search-sticked} ...

positional arguments:
  {evolution,lsd,post-processing,search-sticked}
    evolution           Search with an evolutionary algorithm combining other algorithms
    lsd                 Try to rebuild the building with Line Segment Detector
    post-processing     Apply some kind of orthogonalisation post-processing
    search-sticked      Search for potentially sticked buildings

optional arguments:
  -h, --help            show this help message and exit
```

## Entrées/sorties

Le script lit en entrée un fichier shapefile ou geojson et produit un fichier csv contenant les geometries modifiées.
Il y a quelques limitations :
* Le fichier en entrée ne doit contenir que des polygones ;
* La projection doit être en mètres, seule EPSG:2154 sur la France métropolitaine a été testé ;
* Le seul polygon extérieur est traité, les trous sont ignorés.

## Évolution artificielle

Utilise un algorithme d'évolution artificielle pour combiner plusieurs heuristiques. 

```
$ python3 main.py evolution -h
usage: main.py evolution [-h] [--file FILE] [--key KEY] [--out OUT] [--nb-max NB_MAX] [--nb-generations NB_GENERATIONS] [--nb-per-generation NB_PER_GENERATION]
                         [--mutation_probability MUTATION_PROBABILITY] [--timeout-seconds TIMEOUT_SECONDS] [--cpu CPU]

Search with an evolutionary algorithm combining other algorithms

optional arguments:
  -h, --help            show this help message and exit
  --file FILE, -f FILE  Input polygons file (default: 'input/example.shp')
  --key KEY, -k KEY     Identifiant key in input file (default: 'object_id')
  --out OUT, -o OUT     Output CSV file (default: 'csv/evolution.csv')
  --nb-max NB_MAX, -n NB_MAX
                        Max number of objects treated (default: -)
  --nb-generations NB_GENERATIONS, -g NB_GENERATIONS
                        Number of generations (default: 40)
  --nb-per-generation NB_PER_GENERATION, -p NB_PER_GENERATION
                        Number of objects per generation (default: 50)
  --mutation_probability MUTATION_PROBABILITY, -m MUTATION_PROBABILITY
                        mutation probability per generation (default: 0.5)
  --timeout-seconds TIMEOUT_SECONDS, -t TIMEOUT_SECONDS
                        Timeout per polygon (default: -)
  --cpu CPU, -c CPU     Number of cpu to use (default: 4)
```

## Line Segment Detector

Utilise un serveur WMS ou un fichier jp2 pour construire une vignette
et appliquer l'algorithme Line Segment Detector.
Essaye ensuite de reconstruire la géométrie avec les segments trouvés. 

```
$ python3 main.py lsd -h
usage: main.py lsd [-h] [--file FILE] [--key KEY] [--out OUT] [--segments-file SEGMENTS_FILE] [--vignettes-dir VIGNETTES_DIR] [--wms WMS] [--wms-ident WMS_IDENT]
                   [--wms-layer WMS_LAYER] [--wms-srs WMS_SRS] [--proxy PROXY] [--timeout TIMEOUT] [--scale SCALE] [--nb-max NB_MAX] [--alternative-algo]

Try to rebuild the building with Line Segment Detector

optional arguments:
  -h, --help            show this help message and exit
  --file FILE, -f FILE  Input polygons file (default: 'input/example.shp')
  --key KEY, -k KEY     Identifiant key in input file (default: 'object_id')
  --out OUT, -o OUT     Output CSV file (default: 'csv/LSD.csv')
  --segments-file SEGMENTS_FILE, -g SEGMENTS_FILE
                        Output segments CSV file (default: 'csv/segments.csv')
  --tile-file TILE_FILE, -j TILE_FILE
                        jpeg2000 aerial image (if set, has priority on WMS) (default: -)
  --vignettes-dir VIGNETTES_DIR, -d VIGNETTES_DIR
                        Vignettes storage directory (default: 'vignettes')
  --wms WMS, -w WMS     WMS to get the vignettes (default: 'https://wxs.ign.fr/{IDENT}/geoportail/r/wms')
  --wms-ident WMS_IDENT, -i WMS_IDENT
                        replace {IDENT} in the WMS URL (default: '')
  --wms-layer WMS_LAYER, -l WMS_LAYER
                        Layer name for the WMS (default: 'HR.ORTHOIMAGERY.ORTHOPHOTOS')
  --wms-srs WMS_SRS, -r WMS_SRS
                        Projection for the WMS (default: 'EPSG:2154')
  --proxy PROXY, -p PROXY
                        Proxy for the WMS connection (default: -)
  --timeout TIMEOUT, -t TIMEOUT
                        Timeout in seconds per building (default: 15)
  --scale SCALE, -s SCALE
                        Vignette scale factor (5.0 -> 1px = 20cm) (default: 5.0)
  --nb-max NB_MAX, -n NB_MAX
                        Max number of objects treated (default: -)
  --alternative-algo, -a
                        Try an alternative heuristic search (not ready) (default: False)
```

## Post-traitements

Quelques algorithmes pour nettoyer la géométrie

```
$ python3 main.py post-processing -h
usage: main.py post-processing [-h] [--file FILE] [--out OUT] [--key KEY] [--ortho] [--ortho-step] [--ortho-angle] [--spike] [--use-all]

Apply some kind of orthogonalisation post-processing

optional arguments:
  -h, --help            show this help message and exit
  --file FILE, -f FILE  Input polygons file (default: 'shapefiles/evolution.shp')
  --out OUT, -o OUT     Output CSV file (default: 'csv/post_processing.csv')
  --key KEY, -k KEY     Identifiant key in input file (default: 'BUILDING')
  --ortho, -t           Orthogonalisation post-processing (default: False)
  --ortho-step, -p      Steps orthogonalisation post-processing (default: False)
  --ortho-angle, -g     Angles orthogonalisation post-processing (default: False)
  --spike, -s           Try to delete acute angles (default: False)
  --use-all, --all, -a  All algorithms (default: False)
```

## Bâtiments attachés

Il peut arriver que deux bâtiments voisins soient considérés comme un unique.
On essaye de trouver ceux-ci pour les corriger.

```
$ python3 main.py search-sticked -h
usage: main.py search-sticked [-h] [--file FILE] [--out OUT] [--key KEY] [--split] [--keep] [--coefficient-threshold COEFFICIENT_THRESHOLD]

Search for potentially sticked buildings

optional arguments:
  -h, --help            show this help message and exit
  --file FILE, -f FILE  Input polygons file (default: 'input/example.shp')
  --out OUT, -o OUT     Output CSV file (default: 'csv/sticked.csv')
  --key KEY, -k KEY     Identifiant key in input file (default: 'object_id')
  --split, -s           Try to split the polygon in half (default: False)
  --keep, -p            with -s --split : keep not splitted polygons (default: False)
  --coefficient-threshold COEFFICIENT_THRESHOLD, -c COEFFICIENT_THRESHOLD
                        If above, the building is potentially two (default: 5.0)
```

## Mitoyenneté

Recherche, à partir d'une image aérienne, des segments séparant des bâtiments mitoyens dans le même polygone.

```
$ python main.py adjoining -h
usage: main.py adjoining [-h] [--file FILE] [--tile-file TILE_FILE] [--out OUT] [--key KEY]

    Searches with LSD for segments between two adjoining buildings in the same polygon
    

optional arguments:
  -h, --help            show this help message and exit
  --file FILE, -f FILE  Input polygons file (default: 'input/example.shp')
  --tile-file TILE_FILE, -j TILE_FILE
                        jpeg2000 aerial image (if set, has priority on WMS) (default: 'jp2/example.jp2')
  --out OUT, -o OUT     Output CSV file (default: 'csv/adjoining_segments.csv')
  --key KEY, -k KEY     Identifiant key in input file (default: 'object_id')
```

