#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  Copyright (c) 2021-2021. DGFiP (Direction Générale des Finances Publiques)
#
#  This software is a computer program whose purpose is to find a good
#  representation of buildings from an imprecise one.
#
#  This software is governed by the CeCILL  license under French law and
#  abiding by the rules of distribution of free software.  You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  "http://www.cecill.info".
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using,  modifying and/or developing or reproducing the
#  software by the user in light of its specific status of free software,
#  that may mean  that it is complicated to manipulate,  and  that  also
#  therefore means  that it is reserved for developers  and  experienced
#  professionals having in-depth computer knowledge. Users are therefore
#  encouraged to load and test the software's suitability as regards their
#  requirements in conditions enabling the security of their systems and/or
#  data to be ensured and,  more generally, to use and operate it in the
#  same conditions as regards security.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL license and that you accept its terms.

"""
This Script tries to correct bad cropping of buildings using several heuristics
"""

import math
import os
import time
from concurrent.futures import ProcessPoolExecutor
from os.path import exists
from random import Random
from typing import Dict, Tuple

import numpy as np
import requests
from argh import arg, ArghParser
from owslib.wms import WebMapService
from shapely import geometry
from tqdm import tqdm

import csv
from LSD.lsd_analyse import export_segments, SegmentsFinder, WmsSegmentsFinder, WMSException, ImageSegmentsFinder
from LSD.lsd_search import LSDRoot, LSDNode
from LSD.tree_search import TreeSearch
from evolution.evolution import Evolution, GeneCreationImpossible
from evolution.geometry_gene import GeometryGenerator
from heuristics.post_processing import OrthoPostProcessing, OrthoStepPostProcessing, OrthoAnglePostProcessing, \
    SpikePostProcessing
from utils.functions import read_file
from utils.sticked import sticked_coeff, split_sticked

__author__ = "DGFiP"
__copyright__ = "Copyright 2021, Direction Générale des Finances Publiques"
__license__ = "CeCILL 2.1"
__status__ = "Prototype"


@arg('--file', '-f', help='Input polygons file')
@arg('--key', '-k', help='Identifiant key in input file')
@arg('--out', '-o', help='Output CSV file')
@arg('--nb-max', '-n', type=int, help='Max number of objects treated')
@arg('--nb-generations', '-g', type=int, help='Number of generations')
@arg('--nb-per-generation', '-p', type=int, help='Number of objects per generation')
@arg('--mutation_probability', '-m', type=int, help='mutation probability per generation')
@arg('--timeout-seconds', '-t', type=int, help='Timeout per polygon')
@arg('--cpu', '-c', type=int, help='Number of cpu to use')
def evolution(file: str = 'input/example.shp',
              key: str = 'object_id',
              out: str = 'csv/evolution.csv',
              nb_max: int = None,
              nb_generations: int = 40,
              nb_per_generation: int = 50,
              mutation_probability: float = 0.5,
              timeout_seconds: int = None,
              cpu: int = os.cpu_count()
              ) -> None:
    """Searches with an evolutionary algorithm combining other algorithms"""
    nb_generations = max(nb_generations, 0)
    nb_per_generation = max(nb_per_generation, 1)
    if timeout_seconds:
        timeout_seconds = max(timeout_seconds, 0)
    cpu = max(cpu, 1)

    with open(out, 'w', newline='') as f:
        print(f'Reading {file}')
        buildings_geom = read_file(file, key)

        print('Executing...')
        csv_buildings = csv.writer(f, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        csv_buildings.writerow(['BUILDING', 'EVALUATION', 'GENERATION', 'CALC_TIME', 'GEOM'])

        kwargs_list = [
            {
                'building': building,
                'geom': geom,
                'nb_generations': nb_generations,
                'nb_per_generation': nb_per_generation,
                'mutation_probability': mutation_probability,
                'timeout_seconds': timeout_seconds
            }
            for (building, geom) in buildings_geom.items()
            if geom.area >= 3.0
        ]

        if nb_max:
            kwargs_list = kwargs_list[:nb_max]

        with ProcessPoolExecutor(max_workers=cpu) as ex:
            results = ex.map(run_evolution_parallel, kwargs_list)
            with tqdm(total=len(kwargs_list)) as bar:
                for r in results:
                    bar.update(1)
                    if r[1] < math.inf:
                        csv_buildings.writerow(r)

        with open(out + 't', 'w') as csvt:
            csvt.write('"String(50)","Real","Integer","Integer","WKT"')


def run_evolution(building: str,
                  geom: geometry.Polygon,
                  nb_generations: int = 40,
                  nb_per_generation: int = 50,
                  mutation_probability: float = 0.5,
                  timeout_seconds: int = None
                  ) -> Tuple[str, float, int, int, geometry.Polygon]:
    rng = Random()
    rng.seed(building)
    start = time.time_ns()
    generator = GeometryGenerator(reference=geom, rng=rng)
    try:
        e = Evolution(generator=generator,
                      rng=rng,
                      nb_generations=nb_generations,
                      nb_per_generation=nb_per_generation,
                      mutation_probability=mutation_probability,
                      timeout_seconds=timeout_seconds)
        best_gene, evaluation, best_generation = e.run()
    except GeneCreationImpossible:
        best_gene, evaluation, best_generation = None, math.inf, 0
    delay = round((time.time_ns() - start) / 1000000)
    new_geom = best_gene.geometry.simplify(0.5) if best_gene is not None else geometry.Polygon()
    return building, evaluation, best_generation, delay, new_geom


def run_evolution_parallel(kwargs_list: Dict) -> Tuple[str, float, int, int, geometry.Polygon]:
    return run_evolution(**kwargs_list)


@arg('--file', '-f', help='Input polygons file')
@arg('--key', '-k', help='Identifiant key in input file')
@arg('--out', '-o', help='Output CSV file')
@arg('--segments-file', '-g', help='Output segments CSV file')
@arg('--tile-file', '-j', help='jpeg2000 aerial image (if set, has priority on WMS)')
@arg('--vignettes-dir', '-d', help='Vignettes storage directory')
@arg('--wms', '-w', help="WMS to get the vignettes")
@arg('--wms-ident', '-i', help="replace {IDENT} in the WMS URL")
@arg('--wms-layer', '-l', help="Layer name for the WMS")
@arg('--wms-srs', '-r', help="Projection for the WMS")
@arg('--proxy', '-p', help="Proxy for the WMS connection")
@arg('--timeout', '-t', type=int, help="Timeout in seconds per building")
@arg('--scale', '-s', type=float, help="Vignette scale factor (5.0 -> 1px = 20cm)")
@arg('--nb-max', '-n', type=int, help='Max number of objects treated')
@arg('--alternative-algo', '-a', help="Try an alternative heuristic search (not ready)")
def lsd(file: str = 'input/example.shp',
        key: str = 'object_id',
        out: str = 'csv/LSD.csv',
        segments_file: str = 'csv/segments.csv',
        tile_file: str = None,
        vignettes_dir: str = 'vignettes',
        wms: str = 'https://wxs.ign.fr/inspire/inspire/r/wms',
        wms_ident: str = '',
        wms_layer: str = 'OI.OrthoimageCoverage.HR',
        wms_srs: str = 'EPSG:2154',
        proxy: str = None,
        timeout: int = 15,
        scale: int = 5.0,
        nb_max: int = None,
        alternative_algo: bool = False) -> None:
    """Tries to rebuild the building with Line Segment Detector"""

    if tile_file and exists(tile_file):  # Use tiles
        sf: SegmentsFinder = ImageSegmentsFinder(filename=tile_file)
    else:  # Use WMS
        if proxy:
            os.environ['HTTP_PROXY'] = os.environ['http_proxy'] = proxy
            os.environ['HTTPS_PROXY'] = os.environ['https_proxy'] = proxy

        wms = wms.replace('{IDENT}', wms_ident)
        wms_service = None
        try:
            wms_service = WebMapService(wms, version='1.3.0')
        except requests.exceptions.RequestException:
            print("Can't connect to WMS service")

        sf: SegmentsFinder = WmsSegmentsFinder(wms=wms_service, layer=wms_layer, srs=wms_srs, vignettes_dir=vignettes_dir)
        sf.factor = scale

    with open(out, 'w', newline='') as f:
        csv_bati = csv.writer(f, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csv_bati.writerow(['BUILDING', 'IOU', 'CALC_TIME', 'NB_SEGMENTS', 'GEOM'])

        iou_tab = [0] * 20
        segments_file_opened = False
        print(f'Reading {file}')
        buildings_geom = [(b, g) for b, g in read_file(file, key).items() if g.area >= 100]
        if nb_max:
            buildings_geom = buildings_geom[:nb_max]
        with tqdm(total=len(buildings_geom), desc='Success') as success_bar:
            for building, geom in tqdm(buildings_geom,
                                       total=len(buildings_geom),
                                       desc='Running'):
                sf.building = building
                sf.exterior = geometry.LinearRing(geom.exterior.coords)
                try:
                    segments = sf.find_segments()
                except WMSException:
                    print(f"WMS error with building {building}")
                    continue

                if len(segments) < 3:
                    continue

                export_segments(file=segments_file, segments=segments, building=building,
                                first_call=not segments_file_opened)
                segments_file_opened = True

                max_nb_children = 3
                max_parallel_level = 2
                if len(segments) >= 20:
                    max_nb_children = 2
                    max_parallel_level = 3
                root = LSDRoot(
                    segments=segments,
                    reference=sf.exterior,
                    max_nb_children=max_nb_children,
                    alternative_strategy=alternative_algo
                )
                best_node: LSDNode
                start = time.time_ns()
                best_node, iou = TreeSearch(
                    root=root,
                    max_time_seconds=timeout,
                    max_parallel_level=max_parallel_level).run()
                delay = round((time.time_ns() - start) / 1000000)
                if best_node is not None:
                    final_geom = best_node.get_polygon().simplify(0.2)
                    iou_tab[math.floor(20 * iou)] += 1
                    if iou >= 0.8:
                        success_bar.update(1)
                        csv_bati.writerow([building, iou, delay, len(segments), final_geom.wkt])

        print(f'IOU tab : {iou_tab}')

        with open(out + 't', 'w') as csvt:
            csvt.write('"String(50)","Real","Integer","Integer","WKT"')


@arg('--file', '-f', help='Input polygons file')
@arg('--out', '-o', help='Output CSV file')
@arg('--key', '-k', help='Identifiant key in input file')
@arg('--ortho', '-t', help='Orthogonalisation post-processing')
@arg('--ortho-step', '-p', help='Steps orthogonalisation post-processing')
@arg('--ortho-angle', '-g', help='Angles orthogonalisation post-processing')
@arg('--spike', '-s', help='Try to delete acute angles')
@arg('--use-all', '--all', '-a', help='All algorithms')
def post_processing(file: str = 'shapefiles/evolution.shp',
                    out: str = 'csv/post_processing.csv',
                    key: str = 'BUILDING',
                    ortho: bool = False,
                    ortho_step: bool = False,
                    ortho_angle: bool = False,
                    spike: bool = False,
                    use_all: bool = False) -> None:
    """Applies some kind of orthogonalisation post-processing"""
    print(f'Reading {file}')
    buildings_geom = read_file(file, key)
    with open(out, 'w', newline='') as f:

        print('Executing...')
        csv_buildings = csv.writer(f, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csv_buildings.writerow(['BUILDING', 'GEOM'])

        for building, geom in tqdm(buildings_geom.items()):
            if spike or use_all:
                geom = SpikePostProcessing(iou_threshold=0.97).process(geom)
            if geom and geom.geom_type == 'Polygon' and geom.is_valid and (ortho_step or use_all):
                geom = OrthoStepPostProcessing(length_max=4.0, parallel_threshold_radian=0.25).process(geom)
            if geom and geom.geom_type == 'Polygon' and geom.is_valid and (ortho_angle or use_all):
                geom = OrthoAnglePostProcessing(length_max=4.0, perpendicular_threshold_radian=0.17).process(geom)
            if geom and geom.geom_type == 'Polygon' and geom.is_valid and (ortho or use_all):
                geom = OrthoPostProcessing(distance_max=1.0, cos_threshold=0.2).process(geom)
            if geom and geom.geom_type == 'Polygon' and geom.is_valid and (spike or use_all):
                geom = SpikePostProcessing(iou_threshold=0.97).process(geom)
            if geom and geom.is_valid:
                csv_buildings.writerow([building, geom])

        with open(out + 't', 'w') as csvt:
            csvt.write('"String","WKT"')


@arg('--file', '-f', help='Input polygons file')
@arg('--out', '-o', help='Output CSV file')
@arg('--key', '-k', help='Identifiant key in input file')
@arg('--split', '-s', help="Try to split the polygon in half")
@arg('--keep', '-p', help="with -s --split : keep not splitted polygons")
@arg('--coefficient-threshold', '-c', type=float, help='If above, the building is potentially two')
def search_sticked(file: str = 'input/example.shp',
                   out: str = 'csv/sticked.csv',
                   key: str = 'object_id',
                   split: bool = False,
                   keep: bool = False,
                   coefficient_threshold: float = 5.0) -> None:
    """Searches for potentially sticked buildings"""
    print(f'Reading {file}')
    buildings_geom = read_file(file, key)
    with open(out, 'w', newline='') as f:

        print('Executing...')
        csv_buildings = csv.writer(f, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csv_buildings.writerow(('BUILDING', 'STICKED_COEFF', 'GEOM'))

        for building, geom in tqdm(buildings_geom.items()):
            points = np.array(geom.simplify(0.2).exterior.coords)[:-1]
            coefficient, p1, p2 = sticked_coeff(points)
            is_sticked = coefficient >= coefficient_threshold
            if split:
                treated = False
                if multi := split_sticked(geom, geometry.Point(p1), geometry.Point(p2)):
                    if is_sticked:
                        for i, g in enumerate(multi.geoms):
                            csv_buildings.writerow((f'{building}_{i + 1}', coefficient, g))
                            treated = True
                if keep and not treated:
                    csv_buildings.writerow((building, coefficient, geom))
            elif is_sticked:
                csv_buildings.writerow((building, coefficient, geom))

        with open(out + 't', 'w') as csvt:
            csvt.write('"String","Real","WKT"')


@arg('--file', '-f', help='Input polygons file')
@arg('--tile-file', '-j', help='jpeg2000 aerial image (if set, has priority on WMS)')
@arg('--out', '-o', help='Output CSV file')
@arg('--key', '-k', help='Identifiant key in input file')
def adjoining(
        file: str = 'input/example.shp',
        tile_file: str = 'jp2/example.jp2',
        out: str = 'csv/adjoining_segments.csv',
        key: str = 'object_id') -> None:
    """
    Searches with LSD for segments between two adjoining buildings in the same polygon
    """
    sf: SegmentsFinder = ImageSegmentsFinder(filename=tile_file)
    with open(out, 'w', newline='') as f:
        csv_seg = csv.writer(f, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csv_seg.writerow(['BUILDING', 'SEGMENT', 'GEOM'])
        buildings_geom = [(b, g) for b, g in read_file(file, key).items() if g.area >= 100]
        for building, geom in tqdm(buildings_geom,
                                   total=len(buildings_geom),
                                   desc='Running'):
            sf.building = building
            sf.exterior = geometry.LinearRing(geom.exterior.coords)

            segments = sf.find_adjoining()

            for i, s in enumerate(segments):
                csv_seg.writerow([building, i, s])

    with open(out + 't', 'w') as csvt:
        csvt.write('"String","Integer","WKT"')


parser = ArghParser()
parser.add_commands([evolution, lsd, post_processing, search_sticked, adjoining])

if __name__ == '__main__':
    parser.dispatch()
