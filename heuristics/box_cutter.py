#  Copyright (c) 2021-2021. DGFiP (Direction Générale des Finances Publiques)
#
#  This software is a computer program whose purpose is to find a good
#  representation of buildings from an imprecise one.
#
#  This software is governed by the CeCILL  license under French law and
#  abiding by the rules of distribution of free software.  You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  "http://www.cecill.info".
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using,  modifying and/or developing or reproducing the
#  software by the user in light of its specific status of free software,
#  that may mean  that it is complicated to manipulate,  and  that  also
#  therefore means  that it is reserved for developers  and  experienced
#  professionals having in-depth computer knowledge. Users are therefore
#  encouraged to load and test the software's suitability as regards their
#  requirements in conditions enabling the security of their systems and/or
#  data to be ensured and,  more generally, to use and operate it in the
#  same conditions as regards security.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL license and that you accept its terms.


from typing import Dict

import numpy as np
from shapely import geometry

from heuristics.post_processing import DiagoPostProcessing, StairsPostProcessing
from heuristics.simplification import GeometrySimplifier, PostProcessing, Parameter, Geometry


class BoxCutter(GeometrySimplifier):
    """
    Cuts the base rectangle into small boxes and test each intersection with the reference polygon
    """
    def _do_simplify(self, reference: geometry.Polygon, rectangle_base: geometry.Polygon) -> Geometry:
        return_geom = None
        side_length = self.get_parameter('side_length')
        p1 = np.array(rectangle_base.exterior.coords[0])
        p2 = np.array(rectangle_base.exterior.coords[1])
        p3 = np.array(rectangle_base.exterior.coords[2])
        len1 = np.linalg.norm(p2 - p1)
        len2 = np.linalg.norm(p3 - p2)
        step1 = int(len1 / side_length) + 1  # Number of steps to cut the axis to have almost the desired length
        step2 = int(len2 / side_length) + 1

        v1 = (p2 - p1) / step1
        v2 = (p3 - p2) / step2

        for i in range(step1):
            for j in range(step2):
                points = [
                    p1 + i * v1 + j * v2,
                    p1 + (i + 1) * v1 + j * v2,
                    p1 + (i + 1) * v1 + (j + 1) * v2,
                    p1 + i * v1 + (j + 1) * v2,
                    p1 + i * v1 + j * v2
                ]

                box = geometry.Polygon(points)  # A small square (in fact a rectangle)
                inter = box.intersection(reference)
                if inter.area > 0.5 * box.area:
                    return_geom = box if return_geom is None else return_geom.union(box)
        if return_geom is not None:
            return_geom = return_geom.simplify(0.1)  # To delete useless points
        return return_geom

    def list_parameters(self) -> Dict[str, Parameter]:
        return {
            'side_length': Parameter(min_value=1.5, max_value=6.0, default_value=2.0)
        }

    def list_post_processing(self) -> Dict[str, PostProcessing]:
        return {
            "stairs": StairsPostProcessing(),
            "diago": DiagoPostProcessing()
        }
