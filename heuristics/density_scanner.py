#  Copyright (c) 2021-2021. DGFiP (Direction Générale des Finances Publiques)
#
#  This software is a computer program whose purpose is to find a good
#  representation of buildings from an imprecise one.
#
#  This software is governed by the CeCILL  license under French law and
#  abiding by the rules of distribution of free software.  You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  "http://www.cecill.info".
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using,  modifying and/or developing or reproducing the
#  software by the user in light of its specific status of free software,
#  that may mean  that it is complicated to manipulate,  and  that  also
#  therefore means  that it is reserved for developers  and  experienced
#  professionals having in-depth computer knowledge. Users are therefore
#  encouraged to load and test the software's suitability as regards their
#  requirements in conditions enabling the security of their systems and/or
#  data to be ensured and,  more generally, to use and operate it in the
#  same conditions as regards security.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL license and that you accept its terms.

from typing import Dict, Tuple, List

import numpy as np
from shapely import geometry

from heuristics.post_processing import SmallDefaultsPostProcessing, DiagoPostProcessing
from heuristics.simplification import GeometrySimplifier, PostProcessing, Parameter, Geometry


class DensityScanner(GeometrySimplifier):
    """
    Cuts the base rectangle following his two axis into small slats and "scan" the density area of the reference in each.
    Then group slats if the density is similar.
    Intersections of those groups make small rectangles tested like BoxCutter
    """
    def _do_simplify(self, reference: geometry.Polygon, rectangle_base: geometry.Polygon) -> Geometry:
        return_geom: Geometry = None

        p1 = np.array(rectangle_base.exterior.coords[0])
        p2 = np.array(rectangle_base.exterior.coords[1])
        p3 = np.array(rectangle_base.exterior.coords[2])

        vx = p2 - p1
        vy = p3 - p2
        len1 = np.linalg.norm(vx)
        len2 = np.linalg.norm(vy)
        vx = vx / len1
        vy = vy / len2

        base_length = self.get_parameter('base_length')
        threshold = self.get_parameter('threshold')
        x_lengths, y_lengths = DensityScanner.__scan_geom(reference, rectangle_base, base_length, threshold)

        for ix, lx in enumerate(x_lengths):
            lx1 = x_lengths[ix - 1] if ix > 0 else 0.0
            for iy, ly in enumerate(y_lengths):
                ly1 = y_lengths[iy - 1] if iy > 0 else 0.0

                points = [
                    p1 + lx1 * vx + ly1 * vy,
                    p1 + lx * vx + ly1 * vy,
                    p1 + lx * vx + ly * vy,
                    p1 + lx1 * vx + ly * vy,
                    p1 + lx1 * vx + ly1 * vy
                ]

                box = geometry.Polygon(points)
                inter = box.intersection(reference)
                if inter.area > 0.5 * box.area:
                    return_geom = box if return_geom is None else return_geom.union(box)
        if return_geom is not None:
            return_geom = return_geom.simplify(0.1)  # To delete useless points
        return return_geom

    def list_parameters(self) -> Dict[str, Parameter]:
        return {
            'base_length': Parameter(min_value=0.25, max_value=1.0, default_value=0.5),
            'threshold': Parameter(min_value=0.03, max_value=0.1, default_value=0.05),
            'small_defaults_length': Parameter(min_value=0.5, max_value=1.5, default_value=1.0)
        }

    def list_post_processing(self) -> Dict[str, PostProcessing]:
        return {
            'small_defaults': SmallDefaultsPostProcessing(length_error=self.get_parameter('small_defaults_length')),
            'diago': DiagoPostProcessing()
        }

    @staticmethod
    def __list_lengths(geom: geometry.Polygon,
                       p0: np.ndarray,
                       v0: np.ndarray,
                       v1: np.ndarray,
                       len0: float,
                       len1: float,
                       base_step: float,
                       threshold: float = 0.05) -> List[float]:
        """
        Cuts v0 axis of length len1 into slats following v1 axis of length len1
        Scan de density of geom on those slats to find a list of lengths cutting v0 axis
        """
        lengths = []
        nb_steps = int(len0 / base_step) + 1
        real_step_length = len0 / nb_steps
        v2 = len1 * v1
        current_scan = None
        last_length = 0.0
        for step in range(nb_steps):
            points_zone_scan = [
                p0 + real_step_length * step * v0,
                p0 + (step + 1) * real_step_length * v0,
                p0 + (step + 1) * real_step_length * v0 + v2,
                p0 + real_step_length * step * v0 + v2,
                p0 + real_step_length * step * v0
            ]
            zone_scan = geometry.Polygon(points_zone_scan)
            scan = zone_scan.intersection(geom).area / zone_scan.area  # slat occupation
            if current_scan is None:
                current_scan = scan
            elif abs(scan - current_scan) >= threshold:  # Change of density
                lengths.append(last_length)
                current_scan = scan
            last_length += real_step_length
        lengths.append(last_length)  # Add the last scan
        return lengths

    @staticmethod
    def __scan_geom(geom: geometry.Polygon,
                    rectangle_base:
                    geometry.Polygon,
                    base_step: float = 0.5,
                    threshold: float = 0.05) -> Tuple[List[float], List[float]]:
        """scans the geometry to find some points of interest"""
        p1 = np.array(rectangle_base.exterior.coords[0])
        p2 = np.array(rectangle_base.exterior.coords[1])
        p3 = np.array(rectangle_base.exterior.coords[2])
        vx = p2 - p1
        vy = p3 - p2
        len1 = np.linalg.norm(vx)
        len2 = np.linalg.norm(vy)
        vx = vx / len1
        vy = vy / len2

        x_lengths = DensityScanner.__list_lengths(geom, p1, vx, vy, len1, len2, base_step, threshold)
        y_lengths = DensityScanner.__list_lengths(geom, p1, vy, vx, len2, len1, base_step, threshold)

        return x_lengths, y_lengths
