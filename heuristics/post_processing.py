#  Copyright (c) 2021-2021. DGFiP (Direction Générale des Finances Publiques)
#
#  This software is a computer program whose purpose is to find a good
#  representation of buildings from an imprecise one.
#
#  This software is governed by the CeCILL  license under French law and
#  abiding by the rules of distribution of free software.  You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  "http://www.cecill.info".
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using,  modifying and/or developing or reproducing the
#  software by the user in light of its specific status of free software,
#  that may mean  that it is complicated to manipulate,  and  that  also
#  therefore means  that it is reserved for developers  and  experienced
#  professionals having in-depth computer knowledge. Users are therefore
#  encouraged to load and test the software's suitability as regards their
#  requirements in conditions enabling the security of their systems and/or
#  data to be ensured and,  more generally, to use and operate it in the
#  same conditions as regards security.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL license and that you accept its terms.
#
#  This software is a computer program whose purpose is to find a good
#  representation of buildings from an imprecise one.
#
#  This software is governed by the CeCILL  license under French law and
#  abiding by the rules of distribution of free software.  You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  "http://www.cecill.info".
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using,  modifying and/or developing or reproducing the
#  software by the user in light of its specific status of free software,
#  that may mean  that it is complicated to manipulate,  and  that  also
#  therefore means  that it is reserved for developers  and  experienced
#  professionals having in-depth computer knowledge. Users are therefore
#  encouraged to load and test the software's suitability as regards their
#  requirements in conditions enabling the security of their systems and/or
#  data to be ensured and,  more generally, to use and operate it in the
#  same conditions as regards security.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL license and that you accept its terms.

import math
from typing import List

import numpy as np
from shapely import geometry

from utils.functions import list_turns, list_angles
from heuristics.simplification import PostProcessing, Geometry


class DiagoPostProcessing(PostProcessing):
    _nb_runs: int

    def __init__(self, nb_runs: int = 1):
        self._nb_runs = max(1, nb_runs)

    def _process(self, simple_geom: Geometry, original_geom: geometry.Polygon) -> Geometry:
        ret = simple_geom
        for _ in range(self._nb_runs):
            final_points = []
            modulo = len(ret.exterior.coords) - 1
            for idx in range(modulo):
                current = geometry.Point(simple_geom.exterior.coords[idx])
                point_ok = True
                if current.intersects(original_geom):
                    before = geometry.Point(simple_geom.exterior.coords[(idx - 1) % modulo])
                    after = geometry.Point(simple_geom.exterior.coords[(idx + 1) % modulo])
                    if before.disjoint(original_geom) and after.disjoint(original_geom):
                        triangle = geometry.Polygon([before, current, after, before])
                        if triangle.area > 0.1 \
                                and triangle.centroid.disjoint(simple_geom) \
                                and triangle.intersection(original_geom).area >= 0.25 * triangle.area:
                            point_ok = False
                if point_ok:
                    final_points.append(current)

            final_points.append(final_points[0])
            ret = geometry.Polygon(final_points)

        if not ret.is_valid:
            return simple_geom
        for interior in simple_geom.interiors:
            difference = geometry.Polygon(interior.coords)
            ret = ret.difference(difference)
        return ret


class StairsPostProcessing(PostProcessing):
    def _process(self, simple_geom: Geometry, original_geom: geometry.Polygon) -> Geometry:

        turns = list_turns(simple_geom)

        stair_points = StairsPostProcessing.__build_stair_points(turns)

        stair_points = StairsPostProcessing.__validate_stair_points(stair_points, simple_geom, original_geom)

        if True not in stair_points:
            return simple_geom
        first = None
        new_poly = []
        for stp, stpval in enumerate(stair_points):
            p = simple_geom.exterior.coords[stp]
            pg = geometry.Point(p[0], p[1])
            if first is None and not stair_points[stp]:
                first = pg
            if not stair_points[stp]:
                new_poly.append(pg)
        new_poly.append(first)
        ret = geometry.Polygon([[p.x, p.y] for p in new_poly])
        if not ret.is_valid:
            return simple_geom
        for interior in simple_geom.interiors:
            difference = geometry.Polygon(interior.coords)
            ret = ret.difference(difference)
        return ret

    @staticmethod
    def __build_stair_points(turns: List[float]) -> List[bool]:
        # association des points part d'un escalier ou pas
        stair_points = [False] * len(turns)
        modulo = len(turns)
        nb_points_needed = 4
        for v, vir in enumerate(turns):  # recherche des schémas droite/gauche/droite/gauche
            vc = vir
            stair_valid = True
            if abs(vc) > 0.5:
                nb_points_stairs = 0
                nb_points_add = 0
                max_p = 10
                while stair_valid and max_p > 0:
                    p_next = (v + nb_points_stairs + 1) % modulo
                    nb_points_add += 1
                    if abs(turns[p_next]) > 0.5:
                        if np.sign(vc) != np.sign(turns[p_next]):
                            nb_points_stairs += 1
                            vc = turns[p_next]
                        else:
                            stair_valid = False
                    max_p -= 1

                if nb_points_needed <= nb_points_stairs:
                    for i in range(nb_points_add):
                        stair_points[(v + i) % len(stair_points)] = True
        return stair_points

    @staticmethod
    def __validate_stair_points(stair_points: List[bool],
                                simple_geom: Geometry,
                                original_geom: geometry.Polygon) -> List[bool]:
        for sp, spval in enumerate(stair_points):  # Recherche les points de la forme extérieur/intérieur/extérieur
            if spval:
                p0 = simple_geom.exterior.coords[sp]
                pm1 = simple_geom.exterior.coords[(sp - 1) % len(stair_points)]
                p1 = simple_geom.exterior.coords[(sp + 1) % len(stair_points)]
                if (geometry.Point(p0[0], p0[1]).disjoint(original_geom)
                        or geometry.Point(pm1[0], pm1[1]).intersects(original_geom)
                        or geometry.Point(p1[0], p1[1]).intersects(original_geom)):
                    stair_points[sp] = False
                else:
                    triangle = geometry.Polygon([pm1, p0, p1, pm1])
                    stair_points[sp] = (triangle.area > 0.1 and
                                        triangle.centroid.disjoint(simple_geom) and
                                        triangle.intersection(original_geom).area >= 0.25 * triangle.area)
        return stair_points


class SmallDefaultsPostProcessing(PostProcessing):
    _length_error: float

    def __init__(self, length_error=1.0):
        self._length_error = length_error

    def _process(self, simple_geom: Geometry, original_geom: geometry.Polygon) -> Geometry:
        if len(simple_geom.exterior.coords) <= 6:
            return simple_geom

        turns = list_turns(simple_geom)  # recherche des virages droite/gauche

        final_points = []
        modulo = len(turns)
        for idx, t in enumerate(turns):
            p = np.array(simple_geom.exterior.coords[idx])
            to_add = True
            point_to_add = p

            # Recherche premier point d'une bosse/d'un trou
            tm1 = turns[(idx - 1) % modulo]
            tp1 = turns[(idx + 1) % modulo]
            tp2 = turns[(idx + 2) % modulo]
            if abs(t - tm1) > 1.0 and abs(t - tp1) < 1.0 and abs(t - tp2) > 1.0:  # gauche/droite/droite/gauche
                pm1 = np.array(simple_geom.exterior.coords[(idx - 1) % modulo])
                pp1 = np.array(simple_geom.exterior.coords[(idx + 1) % modulo])
                pp2 = np.array(simple_geom.exterior.coords[(idx + 2) % modulo])
                if (
                        np.linalg.norm(p - pm1) <= self._length_error
                        and np.linalg.norm(p - pp1) <= self._length_error
                        and np.linalg.norm(pp2 - pp1) <= self._length_error  # Pas trop gros
                ):
                    to_add = False

            if to_add:  # Recherche second point d'une bosse/d'un trou
                tm1 = turns[(idx - 1) % modulo]
                tm2 = turns[(idx - 2) % modulo]
                tp1 = turns[(idx + 1) % modulo]
                if abs(tm2 - tm1) > 1.0 and abs(t - tm1) < 1.0 and abs(t - tp1) > 1.0:  # gauche/droite/droite/gauche
                    pm1 = np.array(simple_geom.exterior.coords[(idx - 1) % modulo])
                    pm2 = np.array(simple_geom.exterior.coords[(idx - 2) % modulo])
                    pp1 = np.array(simple_geom.exterior.coords[(idx + 1) % modulo])
                    if np.linalg.norm(pm2 - pm1) <= self._length_error and np.linalg.norm(  # pas trop gros
                            p - pm1) <= self._length_error and np.linalg.norm(
                        p - pp1) <= self._length_error:
                        to_add = False

            if to_add:  # Recherche point dans un petit escalier
                tm1 = turns[(idx - 1) % modulo]
                tp1 = turns[(idx + 1) % modulo]
                if abs(tm1 - t) > 1.0 and abs(t - tp1) > 1.0:  # gauche/droite/gauche
                    pm1 = np.array(simple_geom.exterior.coords[(idx - 1) % modulo])
                    pp1 = np.array(simple_geom.exterior.coords[(idx + 1) % modulo])
                    if (
                            np.linalg.norm(p - pm1) <= self._length_error
                            and np.linalg.norm(p - pp1) <= self._length_error
                    ):
                        pm2 = np.array(simple_geom.exterior.coords[(idx - 2) % modulo])
                        pp2 = np.array(simple_geom.exterior.coords[(idx + 2) % modulo])
                        if (
                                np.linalg.norm(pm1 - pm2) >= 2 * self._length_error
                                and np.linalg.norm(pp2 - pp1) >= 2 * self._length_error
                        ):
                            # Un seul petit escalier, on le gomme en déplaçant
                            # le point vers le symétrique par rapport à la diagonale
                            vsym = pp1 - pm1
                            vsym = vsym / np.linalg.norm(vsym)
                            vinit = p - pm1
                            cos_len = np.dot(vinit, vsym)
                            point_to_add = p + 2 * (cos_len * vsym - vinit)
                        else:
                            to_add = False  # Plusieurs petits, on prend la diagonale
            if to_add:
                final_points.append(point_to_add)

        if len(final_points) < 4:
            return simple_geom

        final_points.append(final_points[0])

        ret = geometry.Polygon(final_points)
        if not ret.is_valid:
            return simple_geom
        for interior in simple_geom.interiors:
            difference = geometry.Polygon(interior.coords)
            ret = ret.difference(difference)
        return ret.simplify(0.1)


class OrthoPostProcessing(PostProcessing):
    _distance_max: float
    _cos_threshold: float

    def __init__(self, distance_max: float = 1.5, cos_threshold=0.1):
        self._distance_max = distance_max
        self._cos_threshold = cos_threshold

    def _process(self, simple_geom: Geometry, original_geom: geometry.Polygon = None) -> Geometry:
        """Angles orthogonalisation"""
        final_points = np.array(simple_geom.exterior.coords)[:-1]
        ortho_points = []
        for idx, pt in enumerate(final_points):
            before: np.ndarray
            if ortho_points:
                before = ortho_points[-1]
            else:
                before = np.array(final_points[(idx - 1) % len(final_points)])
            current = np.array(pt)
            after = np.array(final_points[(idx + 1) % len(final_points)])
            v0 = before - current
            v1 = after - current
            norm0 = np.linalg.norm(v0)
            norm1 = np.linalg.norm(v1)
            moveto = current  # No move by default
            if norm0 > 0.1 and norm1 > 0.1:
                cos = np.dot(v1, v0) / (norm1 * norm0)
                if abs(cos) < self._cos_threshold:  # 0.1 = angle about beween 85 et 95°
                    center = 0.5 * (before + after)  # cercle with diameter before after
                    radius = 0.5 * np.linalg.norm(before - after)
                    v = current - center
                    dist_center = np.linalg.norm(v)
                    move_length = dist_center - radius
                    move_length = np.sign(move_length) * min(abs(move_length), self._distance_max)
                    moveto = current - move_length / dist_center * v
                    if abs(radius - dist_center) <= self._distance_max:  # project on the circle
                        moveto = center + radius / dist_center * v  # dist_center can't be 0 because cos small
            ortho_points.append(moveto)

        ortho_points.append(ortho_points[0])
        return geometry.Polygon(ortho_points).simplify(0.2)


class OrthoStepPostProcessing(PostProcessing):
    """Tries to correct _/\u203E to _|\u203E"""
    _length_max_square: float
    _parallel_threshold_radian: float

    def __init__(self,
                 length_max: float = 4.0,
                 parallel_threshold_radian: float = 0.17  # 0.17 = about 10°
                 ):
        self._length_max_square = length_max ** 2
        self._parallel_threshold_radian = parallel_threshold_radian

    def _process(self, simple_geom: Geometry, original_geom: geometry.Polygon = None) -> Geometry:
        angles = list_angles(simple_geom)
        modulo = len(angles)
        points = np.array(simple_geom.exterior.coords)
        new_points = []
        for i, a in enumerate(angles):
            new_points.append(points[i])
            if (
                    math.pi / 6 < abs(a) < 4 * math.pi / 9
                    and abs(angles[(i + 1) % modulo] + a) <= self._parallel_threshold_radian
            ):
                pm1 = points[i - 1] if i > 0 else points[-2]
                p0 = points[i]
                p1 = points[i + 1]
                pp1 = points[i + 2] if i < modulo - 1 else points[1]
                if np.sum((p0 - p1) ** 2) < self._length_max_square:
                    middle = 0.5 * (p0 + p1)
                    v0 = p0 - pm1
                    v1 = pp1 - p1
                    n0 = np.array([v0[1], -v0[0]])
                    n1 = np.array([v1[1], -v1[0]])
                    try:
                        inter1 = np.linalg.solve([v0, n0], [np.dot(v0, middle), np.dot(n0, p0)])
                        inter2 = np.linalg.solve([v1, n1], [np.dot(v1, middle), np.dot(n1, p1)])
                        new_points.append(inter1)
                        new_points.append(inter2)
                    except np.linalg.LinAlgError:
                        pass
        new_points.append(new_points[0])
        return geometry.Polygon(new_points).simplify(0.2)


class OrthoAnglePostProcessing(PostProcessing):
    """Tries to make sharp angles"""
    _length_max_square: float
    _perpendicular_threshold_radian: float

    def __init__(self, length_max: float = 4.0, perpendicular_threshold_radian: float = 0.17):
        self._length_max_square = length_max ** 2
        self._perpendicular_threshold_radian = perpendicular_threshold_radian

    def _process(self, simple_geom: Geometry, original_geom: geometry.Polygon = None) -> Geometry:
        angles = list_angles(simple_geom)
        modulo = len(angles)
        points = np.array(simple_geom.exterior.coords)
        new_points = []
        for i, a in enumerate(angles):
            new_points.append(points[i])
            if (
                    -math.pi / 2 < a < math.pi / 2
                    and np.sign(a) == np.sign(angles[(i + 1) % modulo])
                    and abs(abs(angles[(i + 1) % modulo] + a) - math.pi / 2) <= self._perpendicular_threshold_radian
            ):
                pm1 = points[i - 1] if i > 0 else points[-2]
                p0 = points[i]
                p1 = points[i + 1]
                pp1 = points[i + 2] if i < modulo - 1 else points[1]
                if np.sum((p0 - p1) ** 2) < self._length_max_square:
                    v0 = p0 - pm1
                    v1 = pp1 - p1
                    n0 = np.array([v0[1], -v0[0]])
                    n1 = np.array([v1[1], -v1[0]])
                    try:
                        inter = np.linalg.solve([n0, n1], [np.dot(n0, p0), np.dot(n1, p1)])
                        new_points.append(inter)
                    except np.linalg.LinAlgError:
                        pass
        new_points.append(new_points[0])
        return geometry.Polygon(new_points).simplify(0.2)


class SpikePostProcessing(PostProcessing):
    """Tries to remove acute angles"""
    _iou_threshold: float

    def __init__(self, iou_threshold: float = 0.97):
        self._iou_threshold = iou_threshold

    def _process(self, simple_geom: Geometry, original_geom: geometry.Polygon = None) -> Geometry:
        angles = list_angles(simple_geom)
        points = np.array(simple_geom.exterior.coords)
        new_points = []
        modulo = len(angles)
        for i, p in enumerate(points[:-1]):
            treated = False
            if abs(angles[i]) >= 2 * math.pi / 3:
                pm1 = points[(i - 1) % modulo]
                pp1 = points[(i + 1) % modulo]
                radius = min(np.linalg.norm(p - pm1), np.linalg.norm(p - pp1))
                circle = geometry.LinearRing(geometry.Point(p).buffer(radius).exterior.coords)
                inter = circle.intersection(geometry.LinearRing(simple_geom.exterior.coords))
                if inter.geom_type == 'MultiPoint' and len(inter.geoms) == 2:
                    p1, p2 = tuple((np.array(pt.coords)[0] for pt in inter.geoms))
                    d1 = min(np.sum((p1 - pm1) ** 2), np.sum((p1 - pp1) ** 2))
                    d2 = min(np.sum((p2 - pm1) ** 2), np.sum((p2 - pp1) ** 2))
                    if d1 < d2:
                        new_points.append(p2)
                    else:
                        new_points.append(p1)
                    treated = True
            if not treated:
                new_points.append(p)
        new_points.append(new_points[0])
        new_geom = geometry.Polygon(new_points).buffer(0).simplify(0.2) if len(new_points) > 4 else simple_geom
        if new_geom.is_valid:
            iou = new_geom.intersection(simple_geom).area / new_geom.union(simple_geom).area
            if iou >= self._iou_threshold:
                return new_geom
        return simple_geom
