#  Copyright (c) 2021-2021. DGFiP (Direction Générale des Finances Publiques)
#
#  This software is a computer program whose purpose is to find a good
#  representation of buildings from an imprecise one.
#
#  This software is governed by the CeCILL  license under French law and
#  abiding by the rules of distribution of free software.  You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  "http://www.cecill.info".
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using,  modifying and/or developing or reproducing the
#  software by the user in light of its specific status of free software,
#  that may mean  that it is complicated to manipulate,  and  that  also
#  therefore means  that it is reserved for developers  and  experienced
#  professionals having in-depth computer knowledge. Users are therefore
#  encouraged to load and test the software's suitability as regards their
#  requirements in conditions enabling the security of their systems and/or
#  data to be ensured and,  more generally, to use and operate it in the
#  same conditions as regards security.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL license and that you accept its terms.
#
#  This software is a computer program whose purpose is to [describe
#  functionalities and technical features of your software].

import math
import sys
from typing import Dict

import numpy as np
import shapely.affinity
from shapely import geometry

from heuristics.simplification import GeometrySimplifier, PostProcessing, Parameter, Geometry


class RectangleMinimumSimplifier(GeometrySimplifier):
    """Searches for the minimum area oriented bbox"""
    def _do_simplify(self, reference: geometry.Polygon, rectangle_base: geometry.Polygon) -> geometry.Polygon:
        simplify = self.get_parameter('simplify')
        simplify_geom = reference.simplify(simplify) if simplify else reference
        return simplify_geom.minimum_rotated_rectangle

    def list_parameters(self) -> Dict[str, Parameter]:
        return {
            "simplify": Parameter(min_value=0.0, max_value=1.0)
        }

    def list_post_processing(self) -> Dict[str, PostProcessing]:
        return {}


class RectangleOptimalSimplifier(GeometrySimplifier):
    """Finds an oriented bbox with an election of the rotation angle"""
    def _do_simplify(self, reference: geometry.Polygon, rectangle_base: geometry.Polygon) -> Geometry:
        simplify = self.get_parameter('simplify')
        geom = reference.simplify(simplify) if simplify else reference

        precision = int(self.get_parameter('precision'))
        election = np.array([0.0] * precision)  # List of angles every vectors will vote for

        fuzzy_factor = self.get_parameter('fuzzy_factor')
        fuzzy_interval = int(self.get_parameter('fuzzy_interval'))

        pi_2 = math.pi / 2
        last_coord = None
        for coord in geom.exterior.coords:
            if last_coord is None:
                last_coord = np.array(coord)
            else:
                np_coord = np.array(coord)
                vector = np_coord - last_coord
                norm = np.linalg.norm(vector)  # vote weight
                if norm > sys.float_info.epsilon:
                    angle = np.arctan2(vector[1], vector[0])
                    if angle >= pi_2:
                        angle = angle - pi_2
                    elif angle < 0.0:
                        angle = angle + pi_2 if angle >= -pi_2 else angle + math.pi
                    vote = int(precision * angle / pi_2)
                    for i in range(-fuzzy_interval, fuzzy_interval+1):  # Distribution around the exact angle
                        place = (vote + i) % precision
                        election[place] = election[place] + norm * (fuzzy_factor ** abs(i))

                last_coord = np_coord

        winner = election.argmax() * pi_2 / precision  # Angle with the most votes
        rotation = shapely.affinity.rotate(geom, -winner, use_radians=True)
        envelope = rotation.envelope
        return shapely.affinity.rotate(envelope, winner, use_radians=True)

    def list_parameters(self) -> Dict[str, Parameter]:
        return {
            "simplify": Parameter(min_value=0.0, max_value=1.0),
            "precision": Parameter(min_value=50, max_value=200, default_value=100),
            "fuzzy_factor": Parameter(min_value=0, max_value=1.0, default_value=0.75),
            "fuzzy_interval": Parameter(min_value=0, max_value=50, default_value=10)
        }

    def list_post_processing(self) -> Dict[str, PostProcessing]:
        return {}

