#  Copyright (c) 2021-2021. DGFiP (Direction Générale des Finances Publiques)
#
#  This software is a computer program whose purpose is to find a good
#  representation of buildings from an imprecise one.
#
#  This software is governed by the CeCILL  license under French law and
#  abiding by the rules of distribution of free software.  You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  "http://www.cecill.info".
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using,  modifying and/or developing or reproducing the
#  software by the user in light of its specific status of free software,
#  that may mean  that it is complicated to manipulate,  and  that  also
#  therefore means  that it is reserved for developers  and  experienced
#  professionals having in-depth computer knowledge. Users are therefore
#  encouraged to load and test the software's suitability as regards their
#  requirements in conditions enabling the security of their systems and/or
#  data to be ensured and,  more generally, to use and operate it in the
#  same conditions as regards security.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL license and that you accept its terms.


import math
from typing import Dict, List

import numpy as np
from shapely import geometry

from heuristics.post_processing import OrthoPostProcessing, OrthoStepPostProcessing, SpikePostProcessing
from heuristics.simplification import GeometrySimplifier, PostProcessing, Parameter, Geometry

import warnings

warnings.filterwarnings("ignore")


class BreakingPoints(GeometrySimplifier):
    """
    For each point in the polygon, find two linear regressions, one with following points, one with previous.
    If the angle between those two lines is over a threshold then the point is marked as "breaking point".
    Only one BP is considered if several are close.
    New linear regressions are calculated between BP and the final polygon is made with the lines intersections
    """
    def _do_simplify(self, reference: geometry.Polygon, rectangle_base: geometry.Polygon) -> Geometry:
        distance_max = self.get_parameter('distance_max')
        threshold_angle = self.get_parameter('angle_rad')

        points_angle = [0.0] * (len(reference.exterior.coords) - 1)
        modulo = len(points_angle)
        max_points = max(int(modulo / 5) + 1, 2)

        for idx, point in enumerate(reference.exterior.coords):
            point_ref = np.array(point)

            # after
            angle_after = math.pi / 2
            point_add = point_ref
            x = []
            y = []
            while (len(x) <= 2 or np.linalg.norm(point_add - point_ref) <= distance_max) and len(x) < max_points:
                x.append(point_add[0])
                y.append(point_add[1])
                point_add = np.array(reference.exterior.coords[(idx + len(x)) % modulo])
            try:
                fit = np.polyfit(np.array(x), np.array(y), 1)
                angle_after = np.arctan(fit[0])
            except ValueError:
                pass  # vertical => keeping Pi/2

            # before
            angle_before = math.pi / 2
            point_add = point_ref
            x = []
            y = []
            while (len(x) < 2 or np.linalg.norm(point_add - point_ref) <= distance_max) and len(x) < max_points:
                x.append(point_add[0])
                y.append(point_add[1])
                point_add = np.array(reference.exterior.coords[(idx - len(x)) % modulo])
            try:
                fit = np.polyfit(np.array(x), np.array(y), 1)
                angle_before = np.arctan(fit[0])
            except ValueError:
                pass  # vertical => keeping Pi/2

            angle_diff = abs(angle_after - angle_before)
            angle_diff = min(angle_diff, math.pi - angle_diff)
            if angle_diff > threshold_angle:
                points_angle[idx % modulo] = angle_diff

        if all(p_angle >= threshold_angle for p_angle in points_angle):  # To be sure to leave the loops
            return None

        breaking_points = []
        for i, p_angle in enumerate(points_angle):  # Find the ma angle for every BP zones
            p = np.array(reference.exterior.coords[i])
            is_best = p_angle > threshold_angle
            compare = (i + 1) % modulo
            p_compare = np.array(reference.exterior.coords[compare])
            while is_best and points_angle[compare] >= threshold_angle and np.linalg.norm(
                    p - p_compare) <= distance_max:
                is_best = points_angle[compare] <= p_angle
                compare = (compare + 1) % modulo
                p_compare = np.array(reference.exterior.coords[compare])
            if is_best:
                compare = (i - 1) % modulo
                p_compare = np.array(reference.exterior.coords[compare])
                while is_best and points_angle[compare] >= threshold_angle and np.linalg.norm(
                        p - p_compare) <= distance_max:
                    is_best = points_angle[compare] <= p_angle
                    compare = (compare - 1) % modulo
                    p_compare = np.array(reference.exterior.coords[compare])
            if is_best:
                breaking_points.append(i)

        if len(breaking_points) < 3:
            return None

        lines = []  # Linear regressions from every BP
        for idx, pt in enumerate(breaking_points):
            idx_after = breaking_points[(idx + 1) % len(breaking_points)]
            if idx_after < pt:
                idx_after = idx_after + modulo
            line: List[float]
            x = []
            y = []
            for i in range(pt, idx_after + 1):
                coords = reference.exterior.coords[i % modulo]
                x.append(coords[0])
                y.append(coords[1])
            try:
                fit = np.polyfit(x, y, 1)
                line = [1.0, -fit[0], fit[1]]
            except ValueError:  # too vertical
                fit = np.polyfit(y, x, 1)
                line = [-fit[0], 1.0, fit[1]]
            lines.append(line)

        final_points = []
        for idx, line in enumerate(lines):
            line_before = lines[(idx - 1) % len(lines)]
            pt_current = np.array(reference.exterior.coords[breaking_points[idx]])
            if len(line) < 3 or len(line_before) < 3:
                final_points.append(pt_current)
            else:
                A = np.array([[line[1], line[0]], [line_before[1], line_before[0]]])
                b = np.array([line[2], line_before[2]])
                try:
                    x = np.linalg.solve(A, b)
                    if np.linalg.norm(x - pt_current) <= distance_max:
                        final_points.append(x)  # new point at the intersection of the regressions
                    else:
                        final_points.append(pt_current)
                except np.linalg.LinAlgError:
                    final_points.append(pt_current)

        final_points.append(final_points[0])
        return_geom = geometry.Polygon(final_points)
        return return_geom if return_geom.is_valid else None

    def list_parameters(self) -> Dict[str, Parameter]:
        return {
            'angle_rad': Parameter(min_value=math.pi/12, max_value=math.pi/4, default_value=math.pi/6),
            'distance_max': Parameter(min_value=1.5, max_value=4.0, default_value=2.0)
        }

    def list_post_processing(self) -> Dict[str, PostProcessing]:
        return {
            'ortho': OrthoPostProcessing(distance_max=1.5),
            'step_ortho': OrthoStepPostProcessing(),
            'spike': SpikePostProcessing()
        }