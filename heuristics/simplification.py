#  Copyright (c) 2021-2021. DGFiP (Direction Générale des Finances Publiques)
#
#  This software is a computer program whose purpose is to find a good
#  representation of buildings from an imprecise one.
#
#  This software is governed by the CeCILL  license under French law and
#  abiding by the rules of distribution of free software.  You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  "http://www.cecill.info".
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using,  modifying and/or developing or reproducing the
#  software by the user in light of its specific status of free software,
#  that may mean  that it is complicated to manipulate,  and  that  also
#  therefore means  that it is reserved for developers  and  experienced
#  professionals having in-depth computer knowledge. Users are therefore
#  encouraged to load and test the software's suitability as regards their
#  requirements in conditions enabling the security of their systems and/or
#  data to be ensured and,  more generally, to use and operate it in the
#  same conditions as regards security.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL license and that you accept its terms.


from __future__ import annotations


from abc import ABC, abstractmethod
from dataclasses import dataclass
from functools import reduce
from typing import Optional, Union, Dict, Tuple, List

from shapely import geometry

Geometry = Optional[Union[geometry.Polygon, geometry.MultiPolygon]]


@dataclass
class Parameter:
    """A parameter for the simplifier with min, max and default values"""
    min_value: float
    max_value: float
    default_value: Optional[float] = None


class GeometrySimplifier(ABC):
    """Transforms a geometry to a simpler version"""
    _parameters: Dict[str, float]
    _post_processing: List[PostProcessing]

    def __init__(self):
        params = self.list_parameters()
        self._parameters = {
            name: p.default_value
            for name, p in params.items() if p.default_value is not None
        }
        self._post_processing = []

    def simplify(self, reference: geometry.Polygon, rectangle_base: geometry.Polygon = None) -> Geometry:
        """
        Executes the simplification and apply the post-processing
        :param reference: Polygon to simplify
        :param rectangle_base: Allegedly a good oriented bounding box for the reference
        :return: Simplified polygon
        """
        simple = self._do_simplify(reference, rectangle_base)
        if simple is not None:
            simple = simple.buffer(0)
            return reduce(lambda geom, proc: proc.process(geom, reference), self._post_processing, simple)

    @abstractmethod
    def _do_simplify(self, reference: geometry.Polygon, rectangle_base: geometry.Polygon) -> Geometry:
        """Effective polygon simplification"""
        ...

    @abstractmethod
    def list_parameters(self) -> Dict[str, Parameter]:
        """
        List of parameters need for the algorithm
        :return: Dict 'name':Parameter
        """
        ...

    def set_parameter(self, name: str, value: float):
        """
        Applies a paramter
        :param name: Parameter name
        :param value: Parameter value, should be in the defined range
        """
        params = self.list_parameters()
        param = params[name]
        if param.min_value <= value <= param.max_value:
            self._parameters[name] = value

    def get_parameter(self, name: str) -> Optional[float]:
        """
        Gets a parameter value
        :param name: parameter name
        :return: parameter value
        """
        if name in self._parameters:
            return self._parameters[name]

    @abstractmethod
    def list_post_processing(self) -> Dict[str, PostProcessing]:
        """
        Lists all possible post-processing algorithms
        :return: Dict 'name': PostProcessing
        """
        ...

    def add_post_processing(self, name: str):
        """Enables a post-processing"""
        p = self.list_post_processing()
        if name in p:
            self._post_processing.append(p[name])


class PostProcessing(ABC):
    """Some modifications made after a simplification"""

    def process(self, simple_geom: Geometry, original_geom: geometry.Polygon = None) -> Geometry:
        if simple_geom is not None:
            correction_geom = simple_geom.buffer(0)
            if correction_geom.geom_type == 'Polygon':
                return self._process(correction_geom, original_geom)

    @abstractmethod
    def _process(self, simple_geom: Geometry, original_geom: geometry.Polygon) -> Geometry:
        ...

