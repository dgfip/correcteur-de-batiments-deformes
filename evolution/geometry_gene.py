#  Copyright (c) 2021-2021. DGFiP (Direction Générale des Finances Publiques)
#
#  This software is a computer program whose purpose is to find a good
#  representation of buildings from an imprecise one.
#
#  This software is governed by the CeCILL  license under French law and
#  abiding by the rules of distribution of free software.  You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  "http://www.cecill.info".
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using,  modifying and/or developing or reproducing the
#  software by the user in light of its specific status of free software,
#  that may mean  that it is complicated to manipulate,  and  that  also
#  therefore means  that it is reserved for developers  and  experienced
#  professionals having in-depth computer knowledge. Users are therefore
#  encouraged to load and test the software's suitability as regards their
#  requirements in conditions enabling the security of their systems and/or
#  data to be ensured and,  more generally, to use and operate it in the
#  same conditions as regards security.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL license and that you accept its terms.


from __future__ import annotations

import math
from functools import cached_property
from random import Random
from typing import List, Callable, Optional, Dict

import shapely.ops

from heuristics.box_cutter import BoxCutter
from heuristics.breaking_points import BreakingPoints
from heuristics.density_scanner import DensityScanner
from evolution.evolution import Gene, SpontaneousGenerator

import numpy as np
from shapely import geometry

from heuristics.post_processing import OrthoPostProcessing, OrthoStepPostProcessing, SpikePostProcessing, OrthoAnglePostProcessing
from heuristics.rectangle import RectangleMinimumSimplifier, RectangleOptimalSimplifier
from heuristics.simplification import GeometrySimplifier, PostProcessing


class GeometryGene(Gene):
    """Concrete Gene implementation adapted to buildings geometry"""
    _reference: geometry.Polygon
    _geometry: geometry.Polygon
    _rng = Random()
    _reference_quality: Dict[str, float]

    _rectangle_algorithms: List[GeometrySimplifier]
    _heuristic_algorithms: List[GeometrySimplifier]

    def __init__(self,
                 reference: geometry.Polygon,
                 geom: geometry.Polygon = None,
                 rng: Random = None,
                 reference_quality: Dict[str, float] = None):
        self._rectangle_algorithms = [
            RectangleMinimumSimplifier(),
            RectangleOptimalSimplifier()
        ]
        self._heuristic_algorithms = [
            BoxCutter(),
            DensityScanner(),
            BreakingPoints()
        ]
        if rng:
            self._rng = rng
        if reference.interiors:
            reference = geometry.Polygon(reference.exterior)
        self._reference = reference
        if reference_quality is None:
            self._reference_quality = {
                'points': GeometryGene.__points_quality(self._reference),
                'length': self._reference.simplify(2.0).length
            }
        else:
            self._reference_quality = reference_quality
        self._geometry = self.__create_random_geometry() if geom is None else geom

    @staticmethod
    def __check_result(result: GeometryGene) -> GeometryGene:
        """
        New Gene validation, should be a polygon
        :param result: The new Gene we are trying to validate
        :return: ret (OK) or None (KO)
        """
        if (
                result is not None
                and result._geometry is not None
                and result._geometry.geom_type == 'Polygon'
                and len(np.array(result._geometry.exterior.coords)) >= 5
        ):
            return result

    def mutate(self) -> GeometryGene:
        switch: List[Callable[[GeometryGene], GeometryGene]] = [
            GeometryGene.__mutate_delete,
            GeometryGene.__mutate_densify,
            GeometryGene.__mutate_move,
            GeometryGene.__mutate_post_processing
        ]
        result = GeometryGene.__check_result(switch[self._rng.randint(0, len(switch)-1)](self))
        return result if result is not None else self

    def __mutate_delete(self) -> GeometryGene:
        """Mutation by deleting some points"""
        nb_points = len(np.array(self._geometry.exterior.coords)) - 1
        nb_max_delete = min(3, nb_points - 4)
        geom = self._geometry
        if nb_max_delete > 0:
            sample = self._rng.sample(range(nb_points), k=nb_points - self._rng.randint(1, nb_max_delete))
            points = [p for i, p in enumerate(np.array(geom.exterior.coords)[:-1]) if i in sample]
            points.append(points[0])
            geom = geometry.Polygon(points)
        return GeometryGene(reference=self._reference,
                            geom=geom,
                            rng=self._rng,
                            reference_quality=self._reference_quality)

    def __mutate_densify(self) -> GeometryGene:
        """Mutation by adding some points"""
        points = np.array(self._geometry.exterior.coords)
        new_points = []
        proba = self._rng.randint(1, 3) / len(points)
        contour = geometry.LinearRing(self._reference.exterior.coords)
        for i, p in enumerate(points[:-1]):
            new_points.append(p)
            if self._rng.random() < proba:
                p1 = points[i + 1]
                v = p1 - p
                n = np.array([v[1], -v[0]])
                norm = np.linalg.norm(v)
                n /= norm
                middle = 0.5 * (p + p1)
                line = geometry.LineString([middle + 2.0*n, middle-2.0*n])
                if line.is_valid:
                    intersection = line.intersection(contour)
                    new_p = None
                    if intersection.geom_type == 'Point':
                        new_p = np.array(intersection.coords)[0]
                    elif intersection.geom_type == 'MultiPoint':
                        np_inter = np.array([np.array(p.coords) for p in intersection.geoms])
                        dist_list = (np.sum((p - middle)**2) for p in np_inter)
                        new_p = np_inter[np.argmin(dist_list)][0]
                    if new_p is not None:
                        new_points.append(new_p)
        new_points.append(points[0])
        geom = geometry.Polygon(new_points).simplify(0.2)
        return GeometryGene(reference=self._reference,
                            geom=geom,
                            rng=self._rng,
                            reference_quality=self._reference_quality)

    def __mutate_move(self) -> GeometryGene:
        """Mutation by moving some points"""
        points = np.array(self._geometry.exterior.coords)
        sample = self._rng.sample(range(len(points) - 1), k=self._rng.randint(1, 3))
        new_points = []
        for i, p in enumerate(points):
            if i in sample:
                rho = self._rng.random()
                theta = 2.0 * math.pi * self._rng.random()
                move = np.array([rho * math.cos(theta), rho * math.sin(theta)])
                new_points.append(p + move)
            else:
                new_points.append(p)
        new_points[-1] = new_points[0]
        geom = geometry.Polygon(points).simplify(0.2)
        return GeometryGene(reference=self._reference,
                            geom=geom,
                            rng=self._rng,
                            reference_quality=self._reference_quality)

    def __mutate_post_processing(self) -> GeometryGene:
        """Mutation by passing some post processing algorithms"""
        switch: List[PostProcessing] = [
            OrthoPostProcessing(),
            OrthoStepPostProcessing(),
            OrthoAnglePostProcessing(),
            SpikePostProcessing()
        ]
        geom = switch[self._rng.randint(0, len(switch)-1)].process(self._geometry, self._reference)
        return GeometryGene(reference=self._reference,
                            geom=geom,
                            rng=self._rng,
                            reference_quality=self._reference_quality)

    def crossover(self, other: GeometryGene) -> GeometryGene:
        switch: List[Callable[[GeometryGene, GeometryGene], GeometryGene]] = [
            GeometryGene.__crossover_union,
            GeometryGene.__crossover_intersection,
            GeometryGene.__crossover_split
        ]
        return GeometryGene.__check_result(switch[self._rng.randint(0, 2)](self, other))

    def __crossover_union(self, other: GeometryGene) -> GeometryGene:
        """Reproduction by union of the two geometries"""
        geom = self._geometry.union(other._geometry).simplify(0.2)
        if self._geometry.hausdorff_distance(geom) >= 0.1:
            return GeometryGene(reference=self._reference,
                                geom=geom,
                                rng=self._rng,
                                reference_quality=self._reference_quality)

    def __crossover_intersection(self, other: GeometryGene) -> GeometryGene:
        """Reproduction by intersection of the two geometries"""
        geom = self._geometry.intersection(other._geometry).simplify(0.2)
        if self._geometry.hausdorff_distance(geom) >= 0.1:
            return GeometryGene(reference=self._reference,
                                geom=geom,
                                rng=self._rng,
                                reference_quality=self._reference_quality)

    def __crossover_split(self, other: GeometryGene) -> GeometryGene:
        """Reproduction by spliting and rebuilding following a random axis"""
        geom1 = self._geometry
        geom2 = other._geometry
        origin = np.array(geom1.centroid)
        theta = math.pi * self._rng.random()
        v = np.array([math.cos(theta), math.sin(theta)])
        line = geometry.LineString([origin - 1000 * v, origin + 1000 * v])
        res1 = shapely.ops.split(geom1, line)
        if len(res1.geoms) == 2:
            res2 = shapely.ops.split(geom2, line)
            if len(res2.geoms) == 2:
                retain1 = res1.geoms[0] if res1.geoms[0].area > res1.geoms[1].area else res1.geoms[1]
                retain2 = res2.geoms[0] if res2.geoms[1].hausdorff_distance(retain1) > res2.geoms[0].hausdorff_distance(
                    retain1) else res2.geoms[1]
                geom = retain1.union(retain2).simplify(0.2)
                return GeometryGene(reference=self._reference,
                                    geom=geom,
                                    rng=self._rng,
                                    reference_quality=self._reference_quality)

    @cached_property
    def fitness(self) -> float:
        union = self._geometry.union(self._reference).area
        return self.__calc_fitness(union) if union >= 1.0 else 10.0

    def __calc_fitness(self, union: float) -> float:
        iou = self._geometry.intersection(self._reference).area / union  # Standard IOU
        base = (1 - iou)

        points_quality = GeometryGene.__points_quality(self._geometry)  # Try to find an evaluation of points
        points_quality /= self._reference_quality['points']

        length_quality = abs(self._geometry.length - self._reference_quality['length'])
        length_quality /= self._reference_quality['length']  # Length should be a bit smaller than reference's one

        return base + 0.1 * points_quality + 0.05 * length_quality  # Weights with a wet finger

    @staticmethod
    def __points_quality(geom: geometry.Polygon) -> float:
        """
        Evaluates the quality of the points of the geometry
        A good point has a right angle and long segments attached to it
        :param geom: The geometry to test
        :return: evaluation lower is better
        """
        points = np.array(geom.exterior.coords)
        nb_points = len(points) - 1
        quality = 0.1
        for i, p in enumerate(points[:-1]):
            p0 = points[(i - 1) % nb_points]
            p1 = points[i + 1]
            v0 = p - p0
            v1 = p1 - p
            norm0 = np.linalg.norm(v0)
            norm1 = np.linalg.norm(v1)
            v0 /= norm0
            v1 /= norm1
            cos = abs(np.dot(v0, v1))
            quality += (cos + 1.0) * (norm0 + norm1)  # cos low = right angle = good quality
        return quality

    @property
    def is_viable(self) -> bool:
        return self._geometry is not None and self._geometry.is_valid

    def __create_random_geometry(self) -> geometry.Polygon:
        """
        Creates a random geometry using some heuristics in order to be close to the reference
        :return: The newly created polygon
        """
        n_max = len(self._rectangle_algorithms) - 1
        rectangle_algo = self._rectangle_algorithms[self._rng.randint(0, n_max)]  # Chooses the rectangle algorithm
        for name, param in rectangle_algo.list_parameters().items():
            if param.default_value is None:
                value = self._rng.random() * (param.max_value - param.min_value) + param.min_value
            else:
                value = self._rng.triangular(param.min_value, param.max_value, param.default_value)
            rectangle_algo.set_parameter(name, value)
        rectangle_base = rectangle_algo.simplify(self._reference)

        n_max = len(self._heuristic_algorithms) - 1
        heuristic = self._heuristic_algorithms[self._rng.randint(0, n_max)]  # Chooses a simplification algorithm
        for name, param in heuristic.list_parameters().items():  # Chooses his parameters
            if param.default_value is None:
                value = self._rng.random() * (param.max_value - param.min_value) + param.min_value
            else:
                value = self._rng.triangular(param.min_value, param.max_value, param.default_value)
            heuristic.set_parameter(name, value)
        for name in heuristic.list_post_processing():  # Chooses post-processing
            if bool(self._rng.getrandbits(1)):
                heuristic.add_post_processing(name)
        geom = heuristic.simplify(self._reference, rectangle_base)
        if geom is not None:
            geom = geom.buffer(0)
            if geom.geom_type == 'Polygon':
                return geom

    @property
    def geometry(self):
        return self._geometry


class GeometryGenerator(SpontaneousGenerator):
    _reference: geometry.Polygon
    _rng: Optional[Random]

    def __init__(self, reference: geometry.Polygon, rng: Random = None):
        self._reference = reference
        self._rng = rng

    def generate(self) -> Gene:
        return GeometryGene(reference=self._reference, rng=self._rng)
