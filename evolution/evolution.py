#  Copyright (c) 2021-2021. DGFiP (Direction Générale des Finances Publiques)
#
#  This software is a computer program whose purpose is to find a good
#  representation of buildings from an imprecise one.
#
#  This software is governed by the CeCILL  license under French law and
#  abiding by the rules of distribution of free software.  You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  "http://www.cecill.info".
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using,  modifying and/or developing or reproducing the
#  software by the user in light of its specific status of free software,
#  that may mean  that it is complicated to manipulate,  and  that  also
#  therefore means  that it is reserved for developers  and  experienced
#  professionals having in-depth computer knowledge. Users are therefore
#  encouraged to load and test the software's suitability as regards their
#  requirements in conditions enabling the security of their systems and/or
#  data to be ensured and,  more generally, to use and operate it in the
#  same conditions as regards security.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL license and that you accept its terms.

from __future__ import annotations

import math
import time
from abc import ABC, abstractmethod
from random import Random
from typing import Tuple, List


class Gene(ABC):
    """
    Abstract object we are trying to optimize
    """
    @abstractmethod
    def mutate(self) -> Gene:
        """Small modification of the object"""
        ...

    @abstractmethod
    def crossover(self, other: Gene) -> Gene:
        """
        Combination of two parent objects
        :param other: the other parent
        :return: the child
        """
        ...

    @property
    @abstractmethod
    def fitness(self) -> float:
        """
        The function to optimize
        Lower is better
        """
        ...

    @property
    @abstractmethod
    def is_viable(self) -> bool:
        """Tests if the object is in the domain of analyze"""
        ...


class SpontaneousGenerator(ABC):
    """Gene creation from scratch"""

    @abstractmethod
    def generate(self) -> Gene:
        """Create a new Gene"""
        ...


class Evolution:
    """An artificial evolution algorithm implementation"""

    _best: Gene
    _best_generation: int
    _generation: List[Gene]
    _generator: SpontaneousGenerator
    _nb_generations: int
    _nb_per_generation: int
    _mutation_probability: float
    _timeout_ns: int = None
    _rng = Random()

    def __init__(self,
                 generator: SpontaneousGenerator,
                 nb_generations: int = 50,
                 nb_per_generation: int = 50,
                 mutation_probability: float = 0.5,
                 timeout_seconds: int = None,
                 rng: Random = None):
        self._generation = []
        self._generator = generator
        self._nb_generations = max(nb_generations, 0)
        self._nb_per_generation = max(nb_per_generation, 1)
        if timeout_seconds:
            self._timeout_ns = time.time_ns() + max(timeout_seconds, 1)*1000000000
        nb_max = 2*self._nb_per_generation
        while len(self._generation) < self._nb_per_generation and nb_max >= 0:
            nb_max -= 1
            divine_arrival = self._generator.generate()
            if divine_arrival.is_viable:
                self._generation.append(divine_arrival)
        if not self._generation:
            raise GeneCreationImpossible()
        self._generation.sort(key=lambda g: g.fitness)
        self._best = self._generation[0]
        self._best_generation = 0
        if rng:
            self._rng = rng
        self._mutation_probability = max(0.0, min(mutation_probability, 1.0))

    def __repr__(self):
        from pprint import pformat
        return pformat(vars(self), indent=4, width=1)

    def run(self) -> Tuple[Gene, float, int]:
        """
        Runs evolution through all generations
        :return: The minimum fitness Gene found with his fitness and his generation number
        """
        for n_gen in range(self._nb_generations):
            if self._timeout_ns and time.time_ns() > self._timeout_ns:
                break
            new_generation = self.__build_new_generation()
            new_generation.sort(key=lambda g: g.fitness)
            if new_generation[0].fitness < self._best.fitness:
                self._best = new_generation[0]
                self._best_generation = n_gen+1
            self.__covid(new_generation)
        return self._best, self._best.fitness, self._best_generation

    def __build_new_generation(self) -> List[Gene]:
        """
        Creates the next generation with random mutations and reproductions
        :return: List of new Genes
        """
        new_generation = (gene.mutate() if self._rng.random() < self._mutation_probability else gene
                          for gene in self._generation)
        new_generation = [gene for gene in new_generation if gene is not None and gene.is_viable]
        for _ in range(self._nb_per_generation):
            if self._generation:
                parent1 = self._generation[math.floor(self._rng.triangular(0, len(self._generation)-1, 0))]
                parent2 = self._generation[math.floor(self._rng.triangular(0, len(self._generation)-1, 0))]
                if parent1 is not parent2:
                    child = parent1.crossover(parent2)
                    if child is not None and child.is_viable:
                        new_generation.append(child)
        while len(new_generation) < self._nb_per_generation:
            immigrant = self._generator.generate()
            if immigrant.is_viable:
                new_generation.append(immigrant)
        if self._rng.randint(32, 52) == 42:
            messiah = self._generator.generate()
            if messiah.is_viable:
                new_generation.append(messiah)
        return new_generation

    def __covid(self, generation: List[Gene]):
        """
        Replaces old generation by deleting Genes with (stochastically) less fitness
        :param generation: New generation with hopefully too many elements
        """
        self._generation.clear()
        m = len(generation)
        if m <= self._nb_per_generation:
            self._generation = generation
        else:
            factor = 2.0 * (m - self._nb_per_generation) / (m ** 2 - m)  # see xcas solve(sum(1-k*x, k, 0, m-1) = n, x)
            for i, g in enumerate(generation):
                if self._rng.random() >= i * factor:
                    self._generation.append(g)  # Vaccinated


class GeneCreationImpossible(Exception):
    pass
