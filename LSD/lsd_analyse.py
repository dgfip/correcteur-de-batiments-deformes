#  Copyright (c) 2021-2021. DGFiP (Direction Générale des Finances Publiques)
#
#  This software is a computer program whose purpose is to find a good
#  representation of buildings from an imprecise one.
#
#  This software is governed by the CeCILL  license under French law and
#  abiding by the rules of distribution of free software.  You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  "http://www.cecill.info".
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using,  modifying and/or developing or reproducing the
#  software by the user in light of its specific status of free software,
#  that may mean  that it is complicated to manipulate,  and  that  also
#  therefore means  that it is reserved for developers  and  experienced
#  professionals having in-depth computer knowledge. Users are therefore
#  encouraged to load and test the software's suitability as regards their
#  requirements in conditions enabling the security of their systems and/or
#  data to be ensured and,  more generally, to use and operate it in the
#  same conditions as regards security.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL license and that you accept its terms.


import csv
from abc import ABC, abstractmethod
from os.path import exists
from typing import Tuple, Union, List, Any

import rasterio
import requests
from pylsd import lsd
from rasterio import windows
from shapely import geometry
import shapely.affinity
import shapely.ops
from owslib.wms import WebMapService
import cv2
import numpy as np

from utils.functions import distance_max


def export_segments(file: str, segments: List[geometry.LineString], building, first_call: bool = False) -> None:
    """
    Writes segments in a file (for debug purpose)
    :param file: CSV file for segments
    :param segments: Segments to export
    :param building: Building identifiant
    :param first_call: erase the file and write headers
    """
    mode = 'a'
    if first_call:
        mode = 'w'
        with open(file + 't', 'w', newline='') as csvt:
            csvt.write('"String(50)","Integer","WKT"')

    with open(file, mode, newline='') as f:
        csv_seg = csv.writer(f, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        if first_call:
            csv_seg.writerow(['BUILDING', 'NUM', 'GEOM'])
        for i, s in enumerate(segments):
            csv_seg.writerow([building, i + 1, s])


class SegmentsFinder(ABC):
    """
    Abstract class to find segments on the original image of the geometry
    """
    building: str
    exterior: geometry.LinearRing
    factor: float = 5.0

    @abstractmethod
    def _find_image(self):
        pass

    def find_segments(self) -> List[geometry.LineString]:
        img = self._find_image()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        bbox = self._find_bbox()
        delta = np.array([bbox[0], bbox[1]])
        height, width = img.shape[:2]

        segments = lsd(gray)

        result: List[geometry.LineString] = []
        if segments is not None:
            for i in range(segments.shape[0]):
                x1, y1 = (int(segments[i, 0]), int(segments[i, 1]))
                x2, y2 = (int(segments[i, 2]), int(segments[i, 3]))
                p1 = np.array([x1, height - y1]) / self.factor + delta
                p2 = np.array([x2, height - y2]) / self.factor + delta
                line = geometry.LineString([p1, p2])
                if distance_max(line, self.exterior) < 1.0:
                    result.append(geometry.LineString([p1, p2]))
                else:
                    while line.length > 3:
                        p1, p2 = np.array(line.coords)
                        v = p2 - p1
                        v /= np.linalg.norm(v)
                        line = geometry.LineString([p1 + v, p2 - v])
                        if distance_max(line, self.exterior) < 1.0:
                            result.append(geometry.LineString([p1, p2]))
                            break
        return result

    def find_adjoining(self) -> List[geometry.LineString]:
        img = self._find_image()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        bbox = self._find_bbox()
        delta = np.array([bbox[0], bbox[1]])
        height, width = img.shape[:2]

        segments = lsd(gray)

        result: List[geometry.LineString] = []
        poly = geometry.Polygon(self.exterior.coords)
        if segments is not None:
            for i in range(segments.shape[0]):
                x1, y1 = (int(segments[i, 0]), int(segments[i, 1]))
                x2, y2 = (int(segments[i, 2]), int(segments[i, 3]))
                p1 = np.array([x1, height - y1]) / self.factor + delta
                p2 = np.array([x2, height - y2]) / self.factor + delta
                if geometry.Point(0.5 * (p1 + p2)).within(poly):
                    line = geometry.LineString([p1, p2])
                    extended_line = shapely.affinity.scale(geom=line, xfact=1.1, yfact=1.1, origin='center')
                    sliced_poly = shapely.ops.split(geom=poly, splitter=extended_line)
                    if len(sliced_poly.geoms) >= 2:
                        nb_big = 0
                        for area in (geometry.Polygon(g).area for g in sliced_poly.geoms):
                            if area > 10:
                                nb_big += 1
                            if nb_big == 2:
                                result.append(line)
                                break
        return result

    def _find_bbox(self, factor: float = 1.3) -> Tuple[float, float, float, float]:
        """
        Finds a bbox a bit more larger than the geometry
        :return: BBox
        """
        envelope = self.exterior.envelope
        envelope = shapely.affinity.scale(geom=envelope, xfact=factor, yfact=factor, origin='center')
        return envelope.bounds


class WmsSegmentsFinder(SegmentsFinder):
    """
    Finds the image on a WMS Service
    """
    _wms: WebMapService
    _layer: str
    _srs: str
    _vignettes_dir: str

    def __init__(self, wms: WebMapService, layer: str, srs: str, vignettes_dir: str):
        self._wms = wms
        self._layer = layer
        self._srs = srs
        self._vignettes_dir = vignettes_dir

    def _find_image(self):
        img_file = self.__get_vignette_name()
        if not exists(img_file):
            try:
                self.__find_vignette()
            except requests.exceptions.RequestException:
                raise WMSException
        if not exists(img_file):
            raise WMSException
        return cv2.imread(filename=img_file)

    def __find_vignette(self) -> None:
        """
        Creates the vignette file with a WMS call
        """
        bbox = self._find_bbox()
        px = round(self.factor * (bbox[2] - bbox[0]))
        py = round(self.factor * (bbox[3] - bbox[1]))
        img = self._wms.getmap(layers=[self._layer],
                               srs=self._srs,
                               bbox=bbox,
                               size=(px, py),
                               format='image/png'
                               )
        with open(self.__get_vignette_name(), 'wb') as file:
            file.write(img.read())

    def __get_vignette_name(self) -> str:
        """
        Filename from building ID
        :return: Vignette filename
        """
        return f'{self._vignettes_dir}/{self.building}.png'


class WMSException(Exception):
    """WMS service error"""
    pass


class ImageSegmentsFinder(SegmentsFinder):
    """Searches in a jp2 image"""
    _dataset: Any

    def __init__(self, filename):
        self._dataset = rasterio.open(filename)
        width = self._dataset.width
        height = self._dataset.height
        x_min, _ = self._dataset.transform * (0, 0)
        x_max, _ = self._dataset.transform * (width, height)
        self.factor = width / (x_max - x_min)

    def __del__(self):
        if self._dataset:
            self._dataset.close()

    def _find_image(self):
        bbox = self._find_bbox()
        w = windows.from_bounds(*bbox, self._dataset.transform)
        colors = self._dataset.read([3, 2, 1], window=w, boundless=True)
        return cv2.merge(colors)
