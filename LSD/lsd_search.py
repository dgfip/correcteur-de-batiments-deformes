#  Copyright (c) 2021-2021. DGFiP (Direction Générale des Finances Publiques)
#
#  This software is a computer program whose purpose is to find a good
#  representation of buildings from an imprecise one.
#
#  This software is governed by the CeCILL  license under French law and
#  abiding by the rules of distribution of free software.  You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  "http://www.cecill.info".
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using,  modifying and/or developing or reproducing the
#  software by the user in light of its specific status of free software,
#  that may mean  that it is complicated to manipulate,  and  that  also
#  therefore means  that it is reserved for developers  and  experienced
#  professionals having in-depth computer knowledge. Users are therefore
#  encouraged to load and test the software's suitability as regards their
#  requirements in conditions enabling the security of their systems and/or
#  data to be ensured and,  more generally, to use and operate it in the
#  same conditions as regards security.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL license and that you accept its terms.


from __future__ import annotations

import math
from typing import List, Tuple, Optional
import numpy as np
from shapely import geometry

from LSD.tree_search import Node
from utils.functions import norm_square, calculate_intersection, calculate_iou, SQRT2_2, distance_max


class LSDNode(Node):
    _new_geom: List[np.ndarray]
    _segments: List[geometry.LineString]
    _reference: geometry.LinearRing
    _is_final_node: bool = False
    _max_nb_children: int

    def __init__(self,
                 reference: geometry.LinearRing,
                 segments: List[geometry.LineString],
                 current_geom: List[np.ndarray],
                 max_nb_children: int
                 ):
        """
        None creation
        :param reference: reference geometry
        :param segments: List of remaining segments
        :param current_geom: List of points for the geometry under construction
        :param max_nb_children: Max number of children to generate for the browse of the tree
        """
        self._reference = reference
        self._max_nb_children = max_nb_children
        self._segments = segments
        self._new_geom = current_geom

        if len(self._new_geom) <= 3:
            return

        if np.array_equal(self._new_geom[0], self._new_geom[-2]):  # Si on a ajouté le premier segment
            self._new_geom.pop()
            self._is_final_node = True
        else:
            last = geometry.Point(self._new_geom[-1])
            first_ones = geometry.LineString([self._new_geom[0], self._new_geom[1]])
            if last.intersects(first_ones):  # Si les derniers veulent devenir les premiers
                self._new_geom[0] = self._new_geom[-1]
                self._is_final_node = True
            else:
                before_last = geometry.Point(self._new_geom[-2])
                if before_last.intersects(first_ones):  # Si les derniers veulent devenir les premiers
                    self._new_geom.pop()
                    self._new_geom[0] = self._new_geom[-1]
                    self._is_final_node = True

            if norm_square(self._new_geom[-1] - self._new_geom[0]) < 0.001:  # Si les derniers sont les premiers
                self._new_geom[-1] = self._new_geom[0]
                self._is_final_node = True

    def children(self) -> List[Node]:
        children_list = self.__search_front_children()
        if len(children_list) < self._max_nb_children:
            children_list += [rc for rc in self.__search_rotation_children() if not rc.is_similar(children_list)]
        if not children_list:
            children_list = self.__search_full_turn_children()
        if not children_list:
            children_list = self.__search_leftover_children()
        return children_list[:self._max_nb_children]

    def __search_front_children(self) -> List[Node]:
        """
        Searches for children in the same direction as the last segment
        :return: List of nodes with an additional segment
        """
        children_list: List[Tuple[LSDNode, float]] = []
        p = self._new_geom[-1]  # dernier point
        v = self._new_geom[-1] - self._new_geom[-2]  # dernier vecteur
        norm = np.linalg.norm(v)
        for i, s in enumerate(self._segments):
            p1, p2 = np.array(s.coords)
            v1 = p1 - p
            v2 = p2 - p
            norm1 = np.linalg.norm(v1)
            norm2 = np.linalg.norm(v2)
            cos1 = np.dot(v1, v) / (norm1 * norm) if norm1 > 0.1 else 0.0
            cos2 = np.dot(v2, v) / (norm2 * norm) if norm2 > 0.1 else 0.0
            if cos1 > -0.05 and cos2 > -0.05:  # Si le segment est dans l'espace continuant le segment précédent
                if cos2 < cos1:
                    p1, p2, cos1, cos2 = p2, p1, cos2, cos1
                v_seg = p2 - p1
                cos = np.dot(v_seg, v) / (np.linalg.norm(v_seg) * norm)
                if -SQRT2_2 <= cos <= SQRT2_2:  # Si angle > 45° on cherche une intersection
                    inter = calculate_intersection(p, v, p1, v_seg)
                    if norm_square(inter - p1) > norm_square(inter - p2):
                        p1, p2 = p2, p1
                    dist_inter = max(np.linalg.norm(inter - p), np.linalg.norm(inter - p1))
                    dist_max = max(
                        distance_max(geometry.LineString([p, inter]), self._reference),
                        distance_max(geometry.LineString([p1, inter]), self._reference)
                    )
                    if dist_max < 1.5:
                        end = [inter, p2] if geometry.Point(inter).intersects(geometry.LineString([p1, p2])) else [
                            inter, p1, p2]
                        child = LSDNode(
                            reference=self._reference,
                            current_geom=self._new_geom[:-1] + end,
                            segments=self._create_segments(i),
                            max_nb_children=self._max_nb_children
                        )
                        children_list.append((child, dist_inter + dist_max))
                elif cos > 0:
                    line = geometry.LineString([p, p1])
                    dist_cur = line.length
                    dist_max = distance_max(line, self._reference)
                    if dist_max < 1.5:
                        child = LSDNode(
                            reference=self._reference,
                            current_geom=self._new_geom + [p1, p2],
                            segments=self._create_segments(i),
                            max_nb_children=self._max_nb_children
                        )
                        children_list.append((child, dist_cur + dist_max))
        children_list.sort(key=lambda c: c[1])
        return list(map(lambda c: c[0], children_list))

    def __search_rotation_children(self):
        """
        Searches for the children with an almost right angle with the last segment
        :return: List of nodes with an additional segment
        """
        children_list: List[Tuple[LSDNode, float]] = []
        p = self._new_geom[-1]  # dernier point
        v = self._new_geom[-1] - self._new_geom[-2]  # dernier vecteur
        norm = np.linalg.norm(v)
        for i, s in enumerate(self._segments):
            seg = np.array(s.coords)
            vs = seg[1] - seg[0]
            cos = np.dot(v, vs) / (norm * np.linalg.norm(vs))
            if -0.5 < cos < 0.5:
                inter = calculate_intersection(p, v, seg[0], vs)
                l1 = norm_square(inter - p)
                l2 = norm_square(inter - p + v)
                if l1 < l2:  # Si l'intersection est plus proche du dernier point que de l'avant dernier
                    dist_max = distance_max(geometry.LineString([inter, seg[0]]), self._reference)
                    if dist_max < 1.5:
                        if np.linalg.norm(inter - seg[1]) < np.linalg.norm(inter - seg[0]):
                            seg = np.array([seg[1], seg[0]])
                        end = [inter, seg[1]] if geometry.Point(inter).intersects(geometry.LineString(seg)) else [
                            inter, seg[0], seg[1]]
                        child = LSDNode(
                            reference=self._reference,
                            current_geom=self._new_geom[:-1] + end,
                            segments=self._create_segments(i),
                            max_nb_children=self._max_nb_children
                        )
                        children_list.append((child, abs(cos)))
        children_list.sort(key=lambda c: c[1])
        return list(map(lambda c: c[0], children_list))

    def __search_full_turn_children(self):
        """
        Searches for children in the opposite direction with the last segment
        :return: List of nodes with an additional segment
        """
        children_list: List[Tuple[LSDNode, float]] = []
        p = self._new_geom[-1]  # dernier point
        v = self._new_geom[-1] - self._new_geom[-2]  # dernier vecteur
        norm = np.linalg.norm(v)
        for i, s in enumerate(self._segments):
            seg = np.array(s.coords)
            vs = seg[1] - seg[0]
            cos = np.dot(v, vs) / (norm * np.linalg.norm(vs))
            if abs(cos) > 0.85:
                if cos > 0:
                    seg = np.array([seg[1], seg[0]])
                norm2_0 = norm_square(p - seg[0])
                norm2_1 = norm_square(p - seg[1])
                dist_max = distance_max(geometry.LineString([p, seg[0]]), self._reference)
                if norm2_0 > 2.0 and norm2_1 > 2.0 and dist_max < 1.5:
                    child = LSDNode(
                        reference=self._reference,
                        current_geom=self._new_geom + [seg[0], seg[1]],
                        segments=self._create_segments(i),
                        max_nb_children=self._max_nb_children
                    )
                    children_list.append((child, norm2_0))
        children_list.sort(key=lambda c: c[1], reverse=True)
        return list(map(lambda c: c[0], children_list))

    def __search_leftover_children(self):
        """
        Searches for remaining children
        :return: List of nodes with an additional segment
        """
        children_list: List[Tuple[LSDNode, float]] = []
        p = self._new_geom[-1]  # dernier point
        for i, s in enumerate(self._segments):
            seg = np.array(s.coords)

            line = geometry.LineString([p, seg[0]])
            dist = distance_max(line, self._reference)
            if dist < 1.5:
                child = LSDNode(
                    reference=self._reference,
                    current_geom=self._new_geom + [seg[0], seg[1]],
                    segments=self._create_segments(i),
                    max_nb_children=self._max_nb_children
                )
                children_list.append((child, dist))

            line = geometry.LineString([p, seg[1]])
            dist = distance_max(line, self._reference)
            if dist < 1.5:
                child = LSDNode(
                    reference=self._reference,
                    current_geom=self._new_geom + [seg[1], seg[0]],
                    segments=self._create_segments(i),
                    max_nb_children=self._max_nb_children
                )
                children_list.append((child, dist))

        children_list.sort(key=lambda c: c[1], reverse=True)
        return list(map(lambda c: c[0], children_list))

    def _create_segments(self, index: int) -> List[geometry.LineString]:
        """
        Copies the list of segments without one element
        :param index: index of the element to delete
        :return: new list of segments
        """
        new_segment = self._segments.copy()
        del new_segment[index]
        return new_segment

    def evaluate(self) -> float:
        """
        Evaluation by calculating the IOU with the reference geometry
        :return: Evaluation
        """
        if not self.is_final():
            return -math.inf
        polygon = self.get_polygon()
        if not polygon.is_valid:
            return 0.0
        compare = geometry.Polygon(self._reference.coords)
        return calculate_iou(polygon, compare)

    def is_valid(self) -> bool:
        """
        Tests if the last segment cross previous ones
        :return: True = pas de croisement
        """
        if len(self._new_geom) >= 5:
            last_seg = geometry.LineString([self._new_geom[-2], self._new_geom[-1]])
            for p1, p2 in zip(self._new_geom[1:-3], self._new_geom[2:-2]):
                s = geometry.LineString([p1, p2])
                if s.intersects(last_seg):
                    return False
        return True

    def is_final(self) -> bool:
        return self._is_final_node

    def get_polygon(self) -> geometry.Polygon:
        """Conversion to Shapely Polygon"""
        return geometry.Polygon(self._new_geom)

    def is_similar(self, others: List[LSDNode]) -> bool:
        """Tests if the last segment is already present in the list"""
        return any(self is not o and np.array_equal(self._new_geom[-2:], o._new_geom[-2:]) for o in others)


class LSDNodeAlternative(LSDNode):
    """
    Another heuristic strategy
    For now produces an higher calculation time and some bas choices
    Must be reworked.
    """

    def __init__(self, reference: geometry.LinearRing, segments: List[geometry.LineString],
                 current_geom: List[np.ndarray], max_nb_children: int):
        super().__init__(reference, segments, current_geom, max_nb_children)

    def children(self) -> List[Node]:
        children_list: List[Tuple[LSDNode, float]] = []
        p = self._new_geom[-1]  # dernier point
        v = self._new_geom[-1] - self._new_geom[-2]  # dernier vecteur
        norm = np.linalg.norm(v)
        for i, s in enumerate(self._segments):
            p1, p2 = np.array(s.coords)
            d1 = norm_square(p1 - p)
            d2 = norm_square(p2 - p)
            if d2 < d1:
                p1, p2, d1, d2 = p2, p1, d2, d1
            v_seg = p2 - p1
            cos = np.dot(v_seg, v) / (np.linalg.norm(v_seg) * norm)
            if abs(cos) > 0.95:
                seg_list, evaluation = self.__search_collinear(p, v, p1, p2)
            else:
                seg_list, evaluation = self.__search_cross(p, v, p1, p2, cos)
            if seg_list:
                child = LSDNodeAlternative(
                    reference=self._reference,
                    current_geom=seg_list,
                    segments=self._create_segments(i),
                    max_nb_children=self._max_nb_children
                )
                children_list.append((child, evaluation))
        children_list.sort(key=lambda c: c[1])
        return list(map(lambda c: c[0], children_list))[:self._max_nb_children]

    def __search_collinear(self,
                           p: np.ndarray,
                           v: np.ndarray,
                           p1: np.ndarray,
                           p2: np.ndarray) -> Tuple[Optional[List[np.ndarray]], float]:
        norm = np.linalg.norm(v)
        v1 = p1 - p
        v2 = p2 - p
        norm1 = np.linalg.norm(v1)
        norm2 = np.linalg.norm(v2)
        cos1 = np.dot(v1, v) / (norm1 * norm) if norm1 > 0.1 else 1.0
        cos2 = np.dot(v2, v) / (norm2 * norm) if norm2 > 0.1 else 1.0
        if cos1 > -0.05 and cos2 > -0.05:  # Si l'autre segment est "visible" du premier
            return self.__search_collinear_accepted(p, v, p1, p2)

        m = 0.5*(p1 + p2)
        n = np.array([v[1], -v[0]])
        inter = calculate_intersection(p, v, m, n)
        if norm_square(inter - m) > 10.0:  # Si le demi-tour est suffisamment loin
            ns, evaluation = self.__search_collinear_accepted(p, v, p1, p2)
            return ns, 2*evaluation
        return None, math.inf

    def __search_collinear_accepted(self,
                                    p: np.ndarray,
                                    v: np.ndarray,
                                    p1: np.ndarray,
                                    p2: np.ndarray) -> Tuple[Optional[List[np.ndarray]], float]:
        new_segments = (None, math.inf)
        vnext = p2 - p1
        n = np.array([v[1], -v[0]])

        d_max = distance_max(geometry.LineString([p, p1]), self._reference)
        if d_max <= 1.5:
            new_segments = (self._new_geom + [p1, p2], d_max)  # Cas liaison directe

        inter = calculate_intersection(p, n, p2, vnext)
        d_max1 = distance_max(geometry.LineString([p, inter, p1]), self._reference)
        end = [p1, p2] if np.dot(inter - p1, inter - p2) >= 0 else [p2]
        if d_max1 <= 1.5 and d_max1 < d_max:
            d_max = d_max1
            new_segments = (self._new_geom + [inter] + end, d_max)

        inter = calculate_intersection(p, v, p1, n)
        d_max2 = distance_max(geometry.LineString([p, inter, p1]), self._reference)
        end_idx = len(self._new_geom) if np.dot(inter - p, inter - p + v) >= 0 else -1
        if d_max2 <= 1.5 and d_max2 < d_max:
            d_max = d_max2
            new_segments = (self._new_geom[:end_idx] + [inter, p1, p2], d_max)

        return new_segments

    def __search_cross(self,
                       p: np.ndarray,
                       v: np.ndarray,
                       p1: np.ndarray,
                       p2: np.ndarray,
                       cos: float) -> Tuple[List[np.ndarray], float]:
        new_segments = (None, math.inf)
        vnext = p2 - p1
        inter = calculate_intersection(p, v, p1, vnext)
        if norm_square(inter - p1) > norm_square(inter - p2):
            p1, p2 = p2, p1
        dot_orig = np.dot(inter - p, inter - p + v) >= 0.0
        dot_end = np.dot(inter - p1, inter - p2) >= 0.0
        p_start = p if dot_orig else inter
        p_end = p1 if dot_end else inter
        d_max = distance_max(geometry.LineString([p_start, inter, p_end]), self._reference)
        if d_max < 1.5:
            end = [inter, p1, p2] if dot_end else [inter, p2]
            new_segments = (self._new_geom[:-1] + end, d_max)

        if abs(cos) > 0.5:
            d_max1 = distance_max(geometry.LineString([p, p1]), self._reference)
            if d_max1 <= 1.5 and d_max1 < d_max:
                new_segments = (self._new_geom + [p1, p2], d_max1)  # Cas liaison directe

        return new_segments


class LSDRoot(Node):
    """
    Root of the search
    """
    _children: List[LSDNode]

    def __init__(self,
                 segments: List[geometry.LineString],
                 reference: geometry.LinearRing,
                 max_nb_children: int = 3,
                 alternative_strategy: bool = False):
        """
        Create a root node
        :param segments: List of segments seen by LSD
        :param reference: Contour of the reference geometry
        :param max_nb_children: Max number of nodes to create for each branch
        """
        i_max = np.argmax([s.length for s in segments])
        seg_max_length = segments[i_max]
        p0, p1 = np.array(seg_max_length.coords)
        kwargs = {
            'reference': reference,
            'segments': segments,
            'current_geom': [p0, p1],
            'max_nb_children': max_nb_children
        }
        children1 = LSDNodeAlternative(**kwargs) if alternative_strategy else LSDNode(**kwargs)

        kwargs = {
            'reference': reference,
            'segments': segments,
            'current_geom': [p1, p0],
            'max_nb_children': max_nb_children
        }
        children2 = LSDNodeAlternative(**kwargs) if alternative_strategy else LSDNode(**kwargs)

        self._children = [children1, children2]

    def children(self) -> List[Node]:
        return self._children

    def evaluate(self) -> float:
        return 0.0

    def is_valid(self) -> bool:
        return True

    def is_final(self) -> bool:
        return False
