#  Copyright (c) 2021-2021. DGFiP (Direction Générale des Finances Publiques)
#
#  This software is a computer program whose purpose is to find a good
#  representation of buildings from an imprecise one.
#
#  This software is governed by the CeCILL  license under French law and
#  abiding by the rules of distribution of free software.  You can  use,
#  modify and/ or redistribute the software under the terms of the CeCILL
#  license as circulated by CEA, CNRS and INRIA at the following URL
#  "http://www.cecill.info".
#
#  As a counterpart to the access to the source code and  rights to copy,
#  modify and redistribute granted by the license, users are provided only
#  with a limited warranty  and the software's author,  the holder of the
#  economic rights,  and the successive licensors  have only  limited
#  liability.
#
#  In this respect, the user's attention is drawn to the risks associated
#  with loading,  using,  modifying and/or developing or reproducing the
#  software by the user in light of its specific status of free software,
#  that may mean  that it is complicated to manipulate,  and  that  also
#  therefore means  that it is reserved for developers  and  experienced
#  professionals having in-depth computer knowledge. Users are therefore
#  encouraged to load and test the software's suitability as regards their
#  requirements in conditions enabling the security of their systems and/or
#  data to be ensured and,  more generally, to use and operate it in the
#  same conditions as regards security.
#
#  The fact that you are presently reading this means that you have had
#  knowledge of the CeCILL license and that you accept its terms.


from __future__ import annotations

import concurrent.futures
import math
import time
from abc import ABC, abstractmethod
from typing import List, Tuple, Optional


class Node(ABC):
    """Abstract class representing a node in the tree"""

    @abstractmethod
    def children(self) -> List[Node]:
        """
        Looks for sub-nodes
        :return: Nodes list, empty list = final node
        """
        ...

    @abstractmethod
    def evaluate(self) -> float:
        """
        Evaluates the node quality
        :return: evaluation
        """
        ...

    @abstractmethod
    def is_final(self) -> bool:
        """
        Tests if the node can be evaluated
        :return: True = final node, False = children needed
        """

    def is_valid(self) -> bool:
        """
        Checks the validity of the node
        :return:
        """
        return True


class TreeSearch:
    """
    Class looking for the optimal solution, browse a tree recursively
    """
    _root: Node
    _eval_factor: float
    _max_time_nanoseconds: Optional[int]
    _max_parallel_level: int

    def __init__(self,
                 root: Node,
                 higher_is_better: bool = True,
                 max_time_seconds: int = None,
                 max_parallel_level: int = 0):
        self._root = root
        self._eval_factor = 1.0 if higher_is_better else -1.0
        self._max_time_nanoseconds = 1000000000 * max_time_seconds if max_time_seconds else None
        self._max_parallel_level = max_parallel_level

    def run(self) -> Tuple[Optional[Node], float]:
        """
        Runs the search
        :return: Best evaluation node
        """
        return self.__run(node=self._root, start_time=time.time_ns())

    def __run(self,
              node: Node,
              start_time: int,
              level: int = 1) -> Tuple[Optional[Node], float]:
        """
        Recursive search for the best node
        :param node: node to evaluate
        :param start_time: start time in nanosecond
        :return: Best node and his evaluation
        """
        if self._max_time_nanoseconds and (time.time_ns() - start_time) > self._max_time_nanoseconds:
            return None, -self._eval_factor * math.inf

        if node.is_final():
            return node, node.evaluate()

        children = (c for c in node.children() if c.is_valid())

        if level <= self._max_parallel_level:
            with concurrent.futures.ProcessPoolExecutor() as ex:
                kwargs_list = ({'self': self, 'node': c, 'start_time': start_time, 'level': level + 1}
                               for c in children)
                results = ex.map(TreeSearch._run_parallel, kwargs_list)
        else:
            results = (self.__run(node=child, start_time=start_time, level=level + 1) for child in children)

        best = (None, -self._eval_factor * math.inf)
        for node, evaluation in results:
            if node and self._eval_factor * evaluation > best[1]:
                best = (node, evaluation)

        return best

    @staticmethod
    def _run_parallel(kwargs_dict):
        """Trick for a parallel call to __run, global function needed"""
        return TreeSearch.__run(**kwargs_dict)
